> Universidad de San Carlos de Guatemala    

> Facultad de Ingeniería 

> Escuela de Ciencias y Sistemas 

> Arquitectura de computadores y ensambladores 1 

> Segundo Semestre 

> Ing. Otto Rene Escobar Leiva 

> Tutor Académico Oscar Peralta

Oscar Daniel Oliva España  - 201902663

# **PROYECTO 2- Manual de Usuario**


## Cómo USAR

Al abrir la aplicación lo primero que se mostrará en pantalla, serán los datos del creador, deberá de presionar ENTER para continuar.

<div align=center>
    <img src="./img/img1.jpg" width="300px">
</div>

## Menú Inicial
Se mostrará un menu, el cual puede seleccionar "Login", "Registrar" o Salir del programa.
<div align=center>
    <img src="./img/img2.jpg" width="300px">
</div>

## LOGIN
Al seleccionar LOGIN, procederá a pedirle el nombre del usuario, si esta cumple con los requisitos, se procederá a pedir la contraseña.
Indicanle distintos mensajes, en algunos específicos casos:

### Si la contraseña es incorrecta
<div align=center>
    <img src="./img/img3.jpg" width="300px">
</div>

### Si el usuario se encuentra bloqueado, lo cual sucede al fallar "3" veces el ingreso de la contraseña.

<div align=center>
    <img src="./img/img4.jpg" width="300px">
</div>

### Si la contraseña es incorrecta y el usuario de tipo "administrador" se encuentra bloqueado.
<div align=center>
    <img src="./img/img5.jpg" width="300px">
</div>

### Por si el usuario no existe 
<div align=center>
    <img src="./img/img6.jpg" width="300px">
</div>

### O si se ingresan correctamene las credencialees

<div align=center>
    <img src="./img/img7.jpg" width="300px">
</div>

## *Dentro de Login*
Pueden haber 3 tipos de usuarios, los cuales se hablarán respectivamente en su menú propio,

# Menú de Usuario

<div align=center>
    <img src="./img/img8.jpg" width="300px">
</div>

# Menú de Administrador
<div align=center>
    <img src="./img/img9.jpg" width="300px">
</div>

Al seleccionar la opción "TOP 10 GLOBAL", se mostrará el Top 10 de las mejores puntuaciones de entre todos los usuarios.

<div align=center>
    <img src="./img/img10.jpg" width="300px">
</div>

Al seleccionar la opción "TOP 10 PERSONAL", se mostrará el Top 10 de las mejores puntuaciones de entre todos los usuarios.

<div align=center>
    <img src="./img/img11.jpg" width="300px">
</div>

# JUGAR
La función principal del proyecto, es el del juego de Tetris, el cual al presionarlo como opción, inmediatamente empezará a jugar el mismo.

En la pantalla se mostrarán todos los puntos obtenidos, el nivel en el que se encuentra, su tablero de juego, la pieza
<div align=center>
    <img src="./img/img16.jpg" width="300px">
</div>

# PAUSA
Al presionar la tecla "ESC" a medio juego, se entrará en el estado de "Pausa", en el cual se le indicarán ciertas acciones que se pueden realizar en este menú, así como el tiempo pausado, el nivel y el punteo actual.

<div align=center>
    <img src="./img/img17.jpg" width="300px">
</div>
# PAUSA
Al llegar a superar niveles, se le mostrará una pantalla de espera, esperando a que presione la "BARRA ESPACIADORA", para poder continuar con el juego en el siguiente nivel.

<div align=center>
    <img src="./img/img18.jpg" width="300px">
</div>

# Menú Administrador Maestro
<div align=center>
    <img src="./img/img12.jpg" width="300px">
</div>

## Desbloquear Usuario
Debido a que si se cometen 3 equivocaciones al introducir la contraseña, el usuario se quedará bloqueado, hasta que un usuario administrador lo desbloquee.
<div align=center>
    <img src="./img/img13.jpg" width="300px">
</div>

# Promover Usuario
Si se logra promover a un usuario, este ahora tendrá las capacidades de un usuario administrador común.
<div align=center>
    <img src="./img/img15.jpg" width="300px">
</div>

# Degradar Usuario
Si se logra degradar a un usuario, este ahora tendrá las capacidades de un usuario común.
<div align=center>
    <img src="./img/img14.jpg" width="300px">
</div>

# Registrar Usuario
Al estar en el Menú Principal y al seleccionar la opción *"REGISTRAR"*, se le pedirá que ingrese el usuario, y si este cumple con lo solicitado:
- No puede empezar por número
- la longitud debe validarse entre 8 y 15 caracteres
- el nombre del usuario no debe existir
- los únicos caracteres especiales que pueden contener 
- ---  serán:---
- guión
- guion bajo
- punto
- 
Para posteriormente pedirle que ingrese la contraseña, que de igual manera tiene sus propias restricciones:
- debe tener al menos 3 mayúsculas
- debe tener al menos 2 números
- debe tener al menos 2 caracteres especiales de los siguientes:---
- Arroba (@)
- Mayor que (>)
- Numeral (#)
- Más (+)
- asterisco (*)
- La longitud debe validarse entre 16 y 20 caractere

<div align=center>
    <img src="./img/img19.jpg" width="300px">
</div>