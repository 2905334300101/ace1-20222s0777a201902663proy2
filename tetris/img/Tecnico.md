> Universidad de San Carlos de Guatemala    

> Facultad de Ingeniería 

> Escuela de Ciencias y Sistemas 

> Arquitectura de computadores y ensambladores 1 

> Segundo Semestre 

> Ing. Otto Rene Escobar Leiva 

> Tutor Académico Oscar Peralta

Oscar Daniel Oliva España  - 201902663

# **PROYECTO 2- Manual Tecnico**


## Variables

Se utilizaron demasidadas variables, debido a que en el lenguaje ensambladlor no es posible mostrar en pantalla algo que no esté guardado.
Para desmostración, solamente se harán mención de algunas, por ejemplo:
## Variables del tablero
Variables en donde se calculan los caracteres que posee el tablero
<div align=center>
    <img src="./img/img1.jpg" width="300px">
</div>

# Variables para modo Video
Debido a que se utilizó princpalmente el modo video, se necesitaron de diferentes variables para mostrar y manejar en dicho modo.
<div align=center>
    <img src="./img/img2.jpg" width="300px">
</div>

# Variables de Menús
Se manejaron una gran cantidad de menús, con los cuales se pudo adentrar en todo el código.
<div align=center>
    <img src="./img/img4.jpg" width="300px">
</div>

# Variables Generales
Se necesitaron de una basta cantidad de variables generales, como contadores, strings vacíos, etc... con el que el manejo del código se facilitaba en demasía.
<div align=center>
    <img src="./img/img3.jpg" width="300px">
</div>

## Macros
Se utilizaron las macros imprimir, limpiarpantalla, pedirenter
- pedirenter= Funciona para recibir datos del usuario
- limpiarpantalla, funciona para limpiar la pantall
- imprimir, funciona para escribir en pantalla
- dibujarpixel, con la cual pintabamos uno a uno los pixeles necesarios a utilizar en el modo video.

<div align=center>
    <img src="./img/img5.jpg" width="300px">
</div>

## Procesos
Se utilziaron los procesos:
- Compararpoisiones, que funciona para verificar si es posible poner al tetrimino.
- compararcadenas, que funciona para comparar las cadenas ingresadas con las del usuario en texto.
- Main, que contiene todo el flujo principal del programa

El Proceso principal, es el que se maneja en el "JUEGO", que es la que posea toda la funcionalidad y la llamada de las macros necesarias para que el tetris funcione como debe.
<div align=center>
    <img src="./img/img7.jpg" width="300px">
</div>
