;--------------------------------------------------- Macros Menus-----------------------------------------------------
;------------------Menu Segun Usuario
LimpiarTodo macro numero,texto,carcater
    LOCAL L1,L2,L3
    push di
    push ax
    xor di,di
    xor ax,ax

    L1:
    mov al, carcater
    mov texto[di],al

    INC di
    CMP DI,numero
    JE L2
    JMP L1

    L2:

    pop ax
    pop di
endm

pasardatosastring macro 
    local L1,L2,L3,L4
    push si
    push di
    push ax
    xor si,si
    xor ax,ax
    xor di,di
    mov si, 0d
    L1:
        cmp Username[si],"$"
        je L2
        mov al,Username[si] 
        mov Usernameausar[di],al
        inc si
        inc di
        jmp L1

    L2:
        pop ax
        pop di
        pop si
endm

MenusGeneral macro
    LOCAL L1,L2,L3,L4,L5
    ;TipoUsuario ;0 -> Usuaio normal, 1 ->Administrador ,2 -> Master
    cmp TipoUsuario,"0"
    JE L1
    cmp TipoUsuario,"2"
    je L2
    cmp TipoUsuario,"1"
    je L3

    L1:
        MenuUsuario
    L2:
        MenuAdmin
    L3:
        MenuAdnibV2
endm

MenuUsuario macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8,L9  
    L1:
    clearScreen
    println MenuUsuarioString
    printChar 10
    ;println Username
    call GetKey
    cmp ax,3D00h ;F3 Jugar
    je L2
    cmp ax,3E00h ;F4 top 10 general
    je L3
    cmp ax,3F00h ;F5 top 10 personal
    je L4
    cmp ax,4400h ;F10 Cerrar
    je L5
    jmp L1
    
    L2:
    ; jugar
    clearScreen
    call PlayGame
    AgregarPunteo punteo
    jmp L9
    L3:
    ;top 10 general
    ObtenerTop10Global
    call GetKey
    jmp L9
    L4:
    ObtenerTop10
    call GetKey
    jmp L9
    L5:
    jmp menuInicio
    
    L9:
    jmp L1
endm

MenuAdnibV2 macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8
    
    L1:
    clearScreen
    println MenuAdminStringv2
    call GetKey
    cmp ax,3B00h ;F1 bloquear
    je L2
    cmp ax,3C00h ;F2 top 10 general
    je L3
    cmp ax,3D00h ;F3 top 10 personal
    je L4
    cmp ax,3E00h ;F4 bubble
    je L5
    cmp ax,3F00h ;F5 Heap
    je L6
    cmp ax,4000h ;F6 Quick
    je L7
    cmp ax,4100h ;F7 Jugar
    je L10
    cmp ax,4400h ;F10 Cerrar
    je L8
    jmp L1
    
    L2:
    ;desbloquear
    DesbloquearUsuario
    jmp L9
    L3:
    ;top 10 general
    ObtenerTop10Global
    call GetKey
    jmp L9
    L4:
    ; top 10 personal
    ObtenerTop10
    call GetKey
    jmp L9
    L5:
    ;burbuja
    jmp L9
    L6:
    ;heap
    jmp L9
    L7:
    ;quick
    jmp L9
    L10:
    ;jugar
    call PlayGame
    AgregarPunteo punteo
    jmp L9
    L8:
    ;cerrar sescion
    jmp menuInicio

    L9:
    jmp L1
  
endm

MenuAdmin macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8,L9  
    L1:
    clearScreen
    println MenuAdminString
   call GetKey
    cmp ax,3B00h ;F1 bloquear
    je L2
    cmp ax,3C00h ;F2 promover
    je L3
    cmp ax,3D00h ;F3 degradar
    je L4
    cmp ax,3E00h ;F4 bubble
    je L5
    cmp ax,3F00h ;F5 Heap
    je L6
    cmp ax,4000h ;F6 Quick
    je L7
    cmp ax,4400h ;F10 Cerrar
    je L8
    jmp L1
    
    L2:
    DesbloquearUsuario
    call GetKey
    jmp L9
    L3:
    PromoverUsuario
    call GetKey
    jmp L9
    L4:
    DegradarUsuario
    call GetKey
    jmp L9
    L5:
    jmp L9
    L6:
    jmp L9
    L7:
    jmp L9
    L8:
    jmp menuInicio

    L9:
    jmp L1
endm
;------ Funciones Usuario normal
ObtenerTop10 macro 
    LOCAL L1,L2,L3,L4,L5,L6,L7
    ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado
    ;imrpimirtodo SIZEOF punteoUsuarioBuscado,punteoUsuarioBuscado 
    clearScreen
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    println Usernameausar
    printChar 10d ;\n
    println printTop10
    printChar 10d ;\n
    xor si,si
    xor di,di
    inc di

    L1:
    cmp si,20d
    je L2
    
    mov al,punteoUsuarioBuscado[si]
    mov CadenaPunteoTemp[0],al
    inc si

    mov al,punteoUsuarioBuscado[si]
    mov CadenaPunteoTemp[1],al
    inc si

    mov Numtemp,di
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    dwtodb Numtemp,txtSalalida
    imrpimirtodo SIZEOF txtSalalida,txtSalalida; Correlativo
    printChar 46d ;pto
    printChar 45d ;guion
    printChar 45d ;guion
    printChar 32d ;espacio
    print CadenaPunteoTemp
    printChar 10d ; \n

    inc di
    jmp L1
    L2:

    L3:

    L4:
   
endm

ObtenerTop10Global macro
    LOCAL L1,L2,L3,L4,L5,L6,L7
    push si
    push di
    xor di,di
    xor si,si

    L1:
    cmp di,1000d
    je L2
    mov al,AllPunteos[di]
    mov CadenaPunteoTemp[0],al
    inc di
    mov al,AllPunteos[di]
    mov CadenaPunteoTemp[1],al
    inc di
   
    push si
    mov incextemp,2d
    cadenaToSword CadenaPunteoTemp,PunteoTemporal
    pop si

    mov ax, PunteoTemporal
    mov ArrPunteoTodos[si],ax

    add si,4d
    jmp L1

    L2:
    pop di
    pop si
    
    generarArrIndices

    ;Ordenar 
    ReordenarPunteoGeneral ArrPunteoTodos,ArrIndiceTodos,50d
    
    ;Obtener en texto
    TOP10Texto
    
endm

;Rearodenar General
ReordenarPunteoGeneral macro arregloNum,arregloIndice,size
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8,L9
    push si
    push di
    xor si,si
    xor di,di

    ;primer for
    L1:
    cmp si,size
    ;cmp si,11d
    jnb L3

    ;segundo for
    mov incextemp,0d
    xor di,di
    L2:
    
   
    ;println p10
    mov IndiceTemp,size
    sub IndiceTemp,si
    sub IndiceTemp,1d
    cmp di,IndiceTemp
    jnb L4

    push ax
    push bx
    push di
    push cx
    push dx

    mov di,incextemp
    mov ax,arregloNum[di]
    mov bx,arregloNum[di+4]

    mov cx,arregloIndice[di]
    mov dx,arregloIndice[di+4]

    ;if primer > segundo
    cmp ax, bx
    jb L5
    jmp L6
    L5:
        ;TOP10DW
        ;getChar
        ;delayOrdenamiento
        ;clearScreen

        ;intercambiar numero
        mov arregloNum[di],bx
        mov arregloNum[di+4],ax
        ;intercambiar indices
        
        mov arregloIndice[di],dx
        mov arregloIndice[di+4],cx
    L6:
    pop dx
    pop cx
    pop di   
    pop ax
    pop bx
    add incextemp,4d
    inc di
    jmp L2
    L4: ;salida 2do for

    inc si
    jmp L1

    L3: ;salida 1er if
    pop si
    pop di
endm

delayOrdenamiento macro
    LOCAL initdelay,startDelay2,endDelay2
    push si
    xor si,si

    initdelay:
        mov cx,1
    startDelay2:
        cmp cx,90000h
        je endDelay2
        inc cx
        jmp startDelay2
    endDelay2:
        inc si
        cmp si,velocidadOrdenamiento
        jne initdelay
        pop si
        
endm

;------ Funciones Admin
DesbloquearUsuario macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8
    println printmsgDesbloquead
    limparvarslogin
    getUsuario Username
    ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado
    ;nombretemp,bloqeadotemp,tipotemp
    cmp seencontrUsuario,0b
    je L1
    jmp L2

    L1:
    jmp L5

    L2:
    mov al, NoBloqueoBuscado[0]
    cmp al,"3"
    jne L3
    jmp L4

    L3:
    println printNoBlqoueado
    jmp L5

    L4:
    println printBlqoueado
    mov si, punteroiniciaUser
    add si,35d
    mov AllUsers[si],"-"
    editarF rutaUsers,AllUsers, SIZEOF AllUsers

    L5:
    
endm

PromoverUsuario macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8
    println printmsgPromover
    limparvarslogin
    getUsuario Username
    ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado

    cmp seencontrUsuario,0b
    je L1
    jmp L2

    L1:
    jmp L5

    L2:
    mov al, TipoBuscado[0]
    cmp al,"1"
    je L3
    jmp L4

    L3:
    println printNoPromovido
    jmp L5

    L4:
    println printPromovido
    mov si, punteroiniciaUser
    add si,36d
    mov AllUsers[si],"1"
    editarF rutaUsers,AllUsers, SIZEOF AllUsers

    L5:
endm

DegradarUsuario macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8
    println printmsgDegradar
    limparvarslogin
    getUsuario Username
    ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado

    cmp seencontrUsuario,0b
    je L1
    jmp L2

    L1:
    jmp L5

    L2:
    mov al, TipoBuscado[0]
    cmp al,"0"
    je L3
    jmp L4

    L3:
    println printNoDegradado
    jmp L5

    L4:
    println printDegradado
    mov si, punteroiniciaUser
    add si,36d
    mov AllUsers[si],"0"
    editarF rutaUsers,AllUsers, SIZEOF AllUsers

    L5:
endm
;------------Macros utilies
generarArrIndices macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8,L9,l10
    push si
    push di
    xor di,di
    xor si,si

    L1:
    cmp si,50d
    je L2

    mov ax,si
    mov ArrIndiceTodos[di+0],ax
    mov ArrIndiceTodos[di+4],ax
    mov ArrIndiceTodos[di+8],ax
    mov ArrIndiceTodos[di+12],ax
    mov ArrIndiceTodos[di+16],ax
    mov ArrIndiceTodos[di+20],ax
    mov ArrIndiceTodos[di+24],ax
    mov ArrIndiceTodos[di+28],ax
    mov ArrIndiceTodos[di+32],ax
    mov ArrIndiceTodos[di+36],ax

    inc si
    
    add di,40d
    jmp L1

    L2:
    pop di
    pop si
    
endm

reiciarbuscadas macro 
    LOCAL L1,L2,L3,L4,L5,L6,L7
    
    xor si,si
    L1:
    cmp si, 15d
    je L2
    mov nombreUserBuscado[si],"$"
    inc si
    jmp L1

    L2:
    xor si,si
    L3:
    cmp si, 20d
    je L4
    mov punteoUsuarioBuscado[si],"$"
    inc si
    jmp L3

    L4:
    mov NoBloqueoBuscado[0],"-"
    mov TipoBuscado[0],"-"
endm

AgregarPunteo macro punteoAdd
    LOCAL L1,L2,L3,L4,L5,L6,L7
    ;println p1

    ;getChar
    ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado
    ;imrpimirtodo SIZEOF punteoUsuarioBuscado,punteoUsuarioBuscado 
    ;printChar 10d

    push si
    push di
    xor di,di
    xor si,si

    L1:
    cmp di,20d
    je L2
    mov al,punteoUsuarioBuscado[di]
    mov CadenaPunteoTemp[0],al
    inc di
    mov al,punteoUsuarioBuscado[di]
    mov CadenaPunteoTemp[1],al
    inc di
   
    mov incextemp,2d
    push si
    cadenaToSword CadenaPunteoTemp,PunteoTemporal
    pop si

    mov ax, PunteoTemporal
    mov ArrPunteo[si],ax
    ;0000;0000;0000;0000;0000;0000;0000;
    
    add si,4d
    jmp L1

    L2:
    mov ax,punteoAdd
    mov ArrPunteo[si],ax

    pop di
    pop si
    
    Rnumlist ArrPunteo,44d
    ReordenarPunteo ArrPunteo,11d
    CopiarTop10 ArrPunteo
    
    ;println p1
    EscribirPunteos punteroiniciaUser,punteoAEscrbir

endm

ReordenarPunteo macro arregloNum,size
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8,L9
    push si
    push di
    xor si,si
    xor di,di

    ;primer for
    L1:
    cmp si,size
    ;cmp si,11d
    jnb L3

    ;segundo for
    mov incextemp,0d
    xor di,di
    L2:
    mov IndiceTemp,size
    ;mov IndiceTemp,11d
    sub IndiceTemp,si
    sub IndiceTemp,1d
    cmp di,IndiceTemp
    jnb L4

    push ax
    push bx
    push di

    mov di,incextemp
    mov ax,arregloNum[di]
    mov bx,arregloNum[di+4]

    ;if primer > segundo
    cmp ax, bx
    jb L5
    jmp L6
    L5:
        mov arregloNum[di],bx
        mov arregloNum[di+4],ax
    L6:
    pop di   
    pop ax
    pop bx
    add incextemp,4d
    inc di
    jmp L2
    L4: ;salida 2do for

    inc si
    jmp L1

    L3: ;salida 1er if
    pop si
    pop di
endm

Rnumlist macro arregloNum,size
    LOCAL while,final
    xor di,di
    xor si,si
    printChar 10d
    println printRnum

    while:  
        cmp si,size
        ;cmp si,44d
        je final
        mov ax,arregloNum[si]  
        mov Numtemp,ax
        
        dwtodb Numtemp,txtSalalida
        imrpimirtodo SIZEOF txtSalalida,txtSalalida
        
        
        add si,4
        jmp while
       
    final: 
    printChar 10d
endm

CopiarTop10 macro arregloNum
    LOCAL while,final,L1,L2
    xor di,di
    xor si,si

    while:

        cmp si,40d
        je final
        mov ax,arregloNum[si]
        cmp ax,10d
        jb L1

        mov Numtemp,ax
        dwtodb Numtemp,txtSalalida
        imrpimirtodo SIZEOF txtSalalida,txtSalalida

        mov al, txtSalalida[0]
        mov punteoAEscrbir[di],al
        inc di

        mov al, txtSalalida[1]
        mov punteoAEscrbir[di],al
        inc di

        jmp L2
        L1:

        mov Numtemp,ax
        dwtodb Numtemp,txtSalalida
        imrpimirtodo SIZEOF txtSalalida,txtSalalida

        mov al, "0"
        mov punteoAEscrbir[di],al
        inc di

        mov al, txtSalalida[0]
        mov punteoAEscrbir[di],al
        inc di
        L2:

        add si,4
        jmp while

    final:
endm

imprimirTop10Punteos macro
    LOCAL L1,L2,L3,L4,L5,L6,L7
    println p3
    
    
    push si
    push di
    xor si,si
    xor di,di
    L1: 
    cmp si,10d
    je L2
   
    mov ax,ArrPunteo[di]
    PAnyNumber
    printChar 10
    
    inc si
    add di,4d
    jmp L1

    L2:
    getChar
    pop si
    pop di

endm

EscribirPunteos macro puntero, punteosTemp
    LOCAL L1,L2,L3,L4,L5,L6,L7
    push bx
    push ax
    push si
    push di
    xor dx,dx
    xor cx,cx
   

    mov bx,37d
    mov ax,puntero
    div bx
    mov bx,20d
    mul bx
    mov si,ax 
    xor di,di
    L1:
    cmp di,20d
    je L2

    mov al,punteosTemp[di]
    mov AllPunteos[si],al
   
    inc si
    inc di
    jmp L1
    L2:

    L3:
    pop di
    pop si
    pop bx
    pop ax

    imrpimirtodo SIZEOF AllPunteos,AllPunteos
    editarF rutaPunteos,AllPunteos, SIZEOF AllPunteos
endm

ObtenerPunteos macro puntero, punteosTemp
    LOCAL L1,L2,L3,L4,L5,L6,L7
    push bx
    push ax
    push si
    push di

    mov bx,37d
    mov ax,puntero
    div bx
    mov bx,20d
    mul bx
    mov si,ax
    xor di,di
    L1:
    cmp di,20d
    je L2
    mov al, AllPunteos[si]
    mov punteosTemp[di],al
    inc si
    inc di
    jmp L1
    L2:

    L3:
    ;imrpimirtodo SIZEOF punteosTemp,punteosTemp 
    ;getChar
    pop di
    pop si
    pop bx
    pop ax
endm

ObtenerDatosEspecificos macro nombreBuscado , nombretemp,bloqeadotemp,tipotemp,punteotemp
    LOCAL L1,L2,L3,L4,L5,L6,L7
    reiciarbuscadas
    ObtenerPosUsuario nombreBuscado
    cmp seencontrUsuario,0b
    je L1
    jmp L2

    L1:
    println ErrorUsuarioNoExistente
    jmp L5

    L2:
    mov si,punteroiniciaUser
    ObtenerPunteos punteroiniciaUser,punteotemp
    xor di,di
   
    L3:
    cmp di,15d
    je L4
    mov al, AllUsers[si]
    mov nombretemp[di],al
    inc si
    inc di
    jmp L3

    L4:
    inc di
    mov nombretemp[di],"$" 
    
    add si, 20d
    mov al,AllUsers[si]
    mov bloqeadotemp[0] , al

    inc si
    mov al,AllUsers[si]
    mov tipotemp[0],al

    ;printChar NoBloqueoBuscado[0]
    ;printChar TipoBuscado[0]
    ;println nombreUserBuscado
    ;recorrercualuqierdb  SIZEOF nombreUserBuscado,nombreUserBuscado


    L5:
endm

recorrercualuqierdb macro size,texto 
    LOCAL L1,L2,L3,L4,L5,L6,L7
    println p7
    xor si,si
    L1:
    cmp si, size
    je L3
    printChar texto[si]
    printChar 10
    inc si
    jmp L1

    L2:
    L3:

endm

ObtenerPosUsuario macro nombreBuscado
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8
    xor bx,bx 
    xor ax,ax
    xor si,si
    xor di,di
    mov punteroiniciaUser,0d
    jmp L1

    L2:
    inc bx
    mov ax,37d
    mul bx
    mov si,ax
    mov punteroiniciaUser,ax

    cmp bx,50d
    je L3
    xor di,di

    L1:
    cmp di,15d
    je L4

    mov al,AllUsers[si]
    mov ah,nombreBuscado[di]
    inc si
    inc di
    cmp al,ah
    je L1
    jmp L2


    L3:
    mov seencontrUsuario,0b
    jmp L5

    L4:
    mov seencontrUsuario,1b

    L5:
    

endm


imrpimirtodo macro numero,texto
    LOCAL L1,L2,L3
    push di
    push ax
    xor di,di
    xor ax,ax

    L1:
    mov al, texto[di]
    printChar al
    INC di
    CMP DI,numero
    JE L2
    JMP L1

    L2:
    pop ax
    pop di
endm


;Macros Archivos -------------------------------
crearF macro ruta
    mov ah,3ch
    mov cx,0
    ;add nombre,exrension
    mov dx,offset ruta
    int 21h
    jc exit ;si no se pudo crear
    mov bx,ax
    mov ah,3eh ;cierra el archivo
    int 21h
endm

abrirF macro ruta,handle
		mov ah,3dh
		mov al,10b
		lea dx,ruta
		int 21h
		mov handle,ax
		jc exitPrograma
endm

leerF macro numbytes,buffer,handle
    mov ah,3fh
    mov bx,handle
    mov cx,numbytes
    lea dx,buffer
    int 21h
    jc exitPrograma
endm

editarF macro ruta,txt,size
    mov ah,3dh
    mov al,1h
    mov dx,offset ruta
    int 21h
    ;println p1
    jc exitPrograma ;Si hubo error
    ;Escritura de archivo
    mov bx,ax ; mover hadfile
    mov cx,size ;num de caracteres a grabar
    mov dx,offset txt
    mov ah,40h
    int 21h
    cmp cx,ax
    ;println p2
    jne exitPrograma ;error salir
    mov ah,3eh ;Cierre de archivo
    int 21h
    ;jmp submenu
endm

;---------REGISTROS

ValidarRegistro macro
    LOCAL REGISTRAR
    REGISTRAR:
        clearScreen
        print LogUsuario
        getTexto RUsuario
        ;no puede empezar por un numero
        cmp RUsuario[0],30
        jb RegistroFase2
        cmp RUsuario[0],39
        ja RegistroFase2
        print ErrorNoNumber
        jmp menuInicio


    RegistroFase2: ;tamaño entre 8 y 15 caracteres
        sizeCadena RUsuario
        cmp al,8
        jb ErrorLongitudUsuarioMenor
        cmp al,0fh
        ja ErrorLongitudUsuarioMayor
        jmp RegistroFase3

    ErrorLongitudUsuarioMenor:
        print ErrorTamanoUsarioMenor
        jmp menuInicio
    
    ErrorLongitudUsuarioMayor:
        print ErrorTamanoUsarioMayor
        jmp menuInicio


    RegistroFase3:;el nombre de usuario no debe existir
        xor bx,bx ;usuarios revisados
        xor ax,ax
        xor si,si
        xor di,di
        jmp iterarUsuarioComprobandoSiExiste
        saltarUsuario:;para saltar de usuario resetea los indices
        ;indice del nuevo usuario
        inc bx
        mov ax,023h
        mul bx
        mov si,ax

        ;comparar si ya termino con todos los usuarios
        cmp bx,032h;comparar con los 50 usuarios que existen
        je RegistroFase4

        xor di,di
        iterarUsuarioComprobandoSiExiste:
        cmp di,0fh
        je ErrorYaExisteUsuario

        mov al,AllUsers[si]
        mov ah,RUsuario[di]
        inc si
        inc di
        cmp al,ah
        je iterarUsuarioComprobandoSiExiste
        jmp saltarUsuario
        
    ErrorYaExisteUsuario:
        print ErrorUsuaioExsitente
        jmp menuInicio

    RegistroFase4: ;verificar caracteres especiales 
        xor ax,ax
        xor si,si
        iteracionUsuario:
            mov al,RUsuario[si]
            inc si
            cmp al,'$'
            je RegistroFase5 ;la cadena termino

            ;verificar que al -> (A-Za-z|0-9|-|_|.)
            cmp al,'-'
            je iteracionUsuario
            cmp al,'_'
            je iteracionUsuario
            cmp al,'.'
            je iteracionUsuario
            ;es un numero
            cmp al,02fh
            ja esunumero2
            jmp alazeta
            esunumero2:
                cmp al,03ah
                jb iteracionUsuario
                jmp alazeta
            ;A-Z
            alazeta:
            cmp al,040h
            ja esunaletraMayuscula2
            jmp alazeta2
            esunaletraMayuscula2:
                cmp al,05bh
                jb iteracionUsuario
                jmp alazeta2
            ;a-z
            alazeta2:
            cmp al,060h
            ja esunaletraMinuscula2
            jmp ErrorCaracterInvalido
            esunaletraMinuscula2:
                cmp al,07bh
                jb iteracionUsuario
            ;El caracter se permite
            jmp ErrorCaracterInvalido

    ErrorCaracterInvalido:
        print ErrorNoSimbol
        dec si
        printChar8 RUsuario[si]
        jmp menuInicio
            
    ;Todas validaciones de usuario aceptadas,ahroa toca la contraseña=======
    RegistroFase5:;debe tener al menos 3 mayusculas
        print LogPassword
        getTexto RPassword
        xor si,si
        xor ah,ah ; numero de mayusculas

        iteracionPass:
        mov al,RPassword[si]
        cmp al,'$'
        je contarMayusculas
        inc si
        cmp al,040h
        ja esunaletraMayuscula3
        jmp iteracionPass
        esunaletraMayuscula3:
            cmp al,05bh
            jb sumaMayusculas
            jmp iteracionPass
            ;a-z
        
        sumaMayusculas:
            inc ah
            jmp iteracionPass
        contarMayusculas:
            cmp ah,3
            jb ErrorNumeroMayusculas
            jmp RegistroNumeroCaracteresPassword

    ErrorNumeroMayusculas:
        print ErrorNumMayus[1]
        jmp menuInicio

    RegistroNumeroCaracteresPassword: ;tamaño entre 8 y 15 caracteres
        sizeCadena RPassword
        cmp al,16d
        jb ErrorLongitudPassMenor
        cmp al,20d
        ja ErrorLongitudPassMayor
        jmp RegistroFase6

    ErrorLongitudPassMenor:
        print ErrorPasswordCorta
        jmp menuInicio
    
    ErrorLongitudPassMayor:
        print ErrorPasswordLarga
        jmp menuInicio




    RegistroFase6:;debe tener al menos 2 numeros
        xor si,si
        xor ah,ah ; numero de numeros

        iteracionPassNum:
        mov al,RPassword[si]
        cmp al,'$'
        je contarNumeros
        inc si
        cmp al,02fh
        ja esunaletraMayuscula4
        jmp iteracionPassNum
        esunaletraMayuscula4:
            cmp al,03ah
            jb sumaNumeros
            jmp iteracionPassNum
            ;a-z
        
        sumaNumeros:
            inc ah
            jmp iteracionPassNum
        contarNumeros:
            cmp ah,2
            jb ErrorNumeroNumeros
            jmp RegistroFase7

    ErrorNumeroNumeros:
        print ErrorNumNum
        jmp menuInicio


    RegistroFase7:;debe tener al menos 2 caracteres especiales: @,>,#,+,*
        xor si,si
        xor ah,ah

        iteracionEspeciales:
            mov al,RPassword[si]
            cmp al,'$'
            je contarEspeciales
            cmp al,'@'
            je AgregarSignoEspecial
            cmp al,'>'
            je AgregarSignoEspecial
            cmp al,'#'
            je AgregarSignoEspecial
            cmp al,'+'
            je AgregarSignoEspecial
            cmp al,'*'
            je AgregarSignoEspecial
            inc si
            jmp iteracionEspeciales
        AgregarSignoEspecial:
            inc ah
            inc si
            jmp iteracionEspeciales

        contarEspeciales:
            cmp ah,2
            jb errorNumeroEspecialesPass
            jmp RegistroFaseFinalPass
    
    errorNumeroEspecialesPass:
        print ErrorNumEspecialesPass
        jmp menuInicio

    RegistroFaseFinalPass:
    print Felicidades
    getChar

endm

AgregarNuevoUsuario macro
    xor ax,ax
    buscarEspacio
    xor di,di
    AgregarByteUsuario:
        cmp di,15d
        je ResetAgregarPassword
        mov al,RUsuario[di]
        mov AllUsers[si],al
        inc di
        inc si
        jmp AgregarByteUsuario
    ResetAgregarPassword:
    xor di,di
    ;dec si
    AgregarBytePassword:
        cmp di,20d
        je UsuarioAgregadoMensaje
        mov al,RPassword[di]
        mov AllUsers[si],al
        inc di
        inc si
        jmp AgregarBytePassword
    
    UsuarioAgregadoMensaje:
        mov AllUsers[si],"-"
        inc si
        mov AllUsers[si],"0"
        print MsgUsuarioAgregado
        editarF rutaUsers,AllUsers, SIZEOF AllUsers
        jmp menuInicio

endm

buscarEspacio macro
    LOCAL L1,L2,L3,L4,L5,L6,L7,L8,Salir
    xor si,si
    xor bx,bx
    jmp L2

    L1:
    add si,37d

    L2:
    mov al,AllUsers[si]
    cmp al,"$"
    je Salir
    jmp L1

    Salir:
endm

;-------- Liempieza

limpearVariablesRegistro macro
    push ax
    mov al,UsuarioLimpio
    mov RUsuario,al
    pop ax
endm

cleantemocadena macro 
    mov tempcadena, "$"
    xor si,si
    mov incextemp,si 
endm

;---------Login  liempieza

limpearVariablesLogin macro
    push ax
    mov al,PasswordLimpio
    mov RPassword,al
    pop ax
endm

limparvarslogin macro
    LOCAL L1,L2,L3,L4,L5,L6,L7
    push ax
    xor si,si 

    L1:
    CMP si,21d
    je L2
    mov Username[si],"$"
    mov Password[si],"$"
    inc si
    jmp L1
    L2:

    L3:


    pop ax
endm 

;----- Login
LoginFase1 macro;verificar que el usuario exista
        LOCAL L1,L2,L3,L4,L5,L6,XYZ
        xor bx,bx ;usuarios revisados
        xor ax,ax
        xor si,si
        xor di,di
        
        ;println Username
        jmp iterarUsuarioComprobandoSiExisteLogin
        saltarUsuarioLogin:;para saltar de usuario resetea los indices
        ;indice del nuevo usuario
        inc bx
        mov ax,37d
        mul bx
        mov si,ax

        ;comparar si ya termino con todos los usuarios
        cmp bx,50d;comparar con los 50 usuarios que existen
        je ErrorUsuarioNoExiste

        xor di,di
        iterarUsuarioComprobandoSiExisteLogin:
        cmp di,15d
        je LoginFase2

        mov al,AllUsers[si]
        mov ah,Username[di]
        inc si
        inc di
        cmp al,ah
        je iterarUsuarioComprobandoSiExisteLogin
        jmp saltarUsuarioLogin
        
    LoginFase2: ;verificar si la contraseña es la misma
       
        ;println Password
        mov UsuarioActual, bl; Usuario actual
        mov ax,37d ;37 bytes por usuario
        mul bx ; ax = ax * bx -> resulta al inicio del usuario actual
        mov bx,ax ;copiar donde inicia el usuario

        add ax,15d ; aumenta la cantidad de bytes que ocupa el usuario35
        add bx,35d ; aumentar lo que ocupa usuario y password
        mov si,ax
        xor di,di
        iteracionVerificarContrasenia:
            cmp di,20d
            je LoginCorrecto

            mov al,AllUsers[si]
            mov ah,Password[di]
            inc si
            inc di
            cmp al,ah
            je iteracionVerificarContrasenia
            jmp ContraseniaIngresadaErronea
        
    LoginCorrecto:
        push si
        push bx
        ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado
        pop bx
        mov si,bx
        cmp TipoBuscado,"0"
        jne XYZ

        mov al,AllUsers[si]
        cmp al,"3"
        je L5

        XYZ:
        mov AllUsers[si],"-"
        editarF rutaUsers,AllUsers, SIZEOF AllUsers
        inc si
        mov al,AllUsers[si]
        mov TipoUsuario,al
        pop si
        printChar 10
        println plog
        Getiguala charenter
        clearScreen
        
        MenusGeneral
        ;jmp MenusLogins

    ContraseniaIngresadaErronea:
        push si
        mov si,bx
        mov al,AllUsers[si]

        cmp al,"-"
        je L1
        cmp al,"1"
        je L2
        cmp al,"2"
        je L3
        cmp al,"3"
        je L5

        L1: 
        mov AllUsers[si],"1"
        editarF rutaUsers,AllUsers, SIZEOF AllUsers
        jmp L4

        L2: 
        mov AllUsers[si],"2"
        editarF rutaUsers,AllUsers, SIZEOF AllUsers
        jmp L4

        L3: 
        mov AllUsers[si],"3"
        editarF rutaUsers,AllUsers, SIZEOF AllUsers
        jmp L4

        L5:
        pop si
        ObtenerDatosEspecificos Username,nombreUserBuscado,NoBloqueoBuscado,TipoBuscado,punteoUsuarioBuscado
        cmp TipoBuscado,"0"
        jne L6
        println userbloqueado
        jmp menuInicio
        ;jmp L4

        L6:
        println useradminbloqueado
        calcularcooldown
        println useradmindesbloqueado

        
        jmp menuInicio

        L4:
        pop si
       
        println ErrorPasswordNoCorrecta
        jmp menuInicio

    ErrorUsuarioNoExiste:
       
        println ErrorUsuarioNoExistente
        jmp menuInicio

endm

;---------gENERALES

dwtodb macro numero,stringvar
    local cNumerador,Convertir 
    push si
    push ax
    push bx
    push dx
    mov contador1,0d
    xor si,si
    mov bx,10d 
    mov ax,numero
    cNumerador:
        xor dx,dx
        div bx
        push dx
        inc contador1
        cmp ax,0
        jne cNumerador
    mov si,offset stringvar
    Convertir:
        pop dx
        add dx,48d
        mov [si],dx
        inc si
        dec contador1
        cmp contador1,0
        jne Convertir   
    pop dx
    pop bx
    pop ax
    pop si
		;AGREGARSimbolo SIZEOF stringvar,stringvar
        ;println stringvar
endm

AGREGARSimbolo macro numero, texto 
    mov si,numero
    inc si
    mov texto[si],"$"

endm

limpiarpantalla macro
    mov ah, 00h
    mov al, 03h
    int 10h
endm    

;Prin cadena
print macro cadena
    mov ah, 09h
    mov dx, offset cadena
    int 21h
endm

println macro cadena
    print cadena
	printChar 10d
endm

;print numero
printnum macro num
    mov ah,02h
    mov dl,num
    add dl,'0'
    int 21h
endm

;Imprime un caracter
printChar macro char
    mov ah,02h
    mov dl, char
    int 21h
endm

;obtener char
getChar macro
    mov ah, 01h
    int 21h
endm

setGraphicMode macro ;pone en modo grafico el programa
    mov ax, 0013h
    int 10h
endm

setTextMode macro ;pone en modod de texto el programa
    mov ax, 0003h
    int 10h
endm

clearScreen macro	;limpia la pantalla, solo en modo texto
    setGraphicMode
    setTextMode
endm

cadenaToSword macro texto,var
    LOCAL buclerecorrercadena,FinalC
   
    ;print msgE0
    ;print texto
    ;print msgSalto
    mov var,00h
    ;sizeCadena texto 
    mov si,incextemp
    dec si; esto decrementa para que no se pase el indice, el tamaño es mayor al indice
    xor ax,ax   ; ESTA ES LA VARIABLE TEMPORAL
    mov bx,1; inicio el multiplicador en 1
    
    buclerecorrercadena:;recorre la cadena desde laultima posicino hasta la primera    
        mov al,texto[si] ;temporal = entrada[i]
        sub ax,030h ; temporal = temporal - 48
        mul bx; multiplica el valor de la cadena en un espacio por el multiplicador y la coloca bx ; temporal = temporal * multiplicador
        add var, ax; binario+=bx    ; entero = enterio + temporal
        mov ax,0Ah ;incrementar el multiplicador en 10    
        mul bx
        mov bx,ax
        cmp si,00h
        je FinalC
        xor ax,ax
        dec si ; decrementa el contador de la variable    
        jmp buclerecorrercadena
    FinalC:
    
endm

PAnyNumber macro          
    LOCAL label1,print1,salir
    mov cx,0
    mov dx,0
    label1:
        cmp ax,0
        je print1     
        mov bx,10       
        div bx                   
        push dx             
        inc cx             
        xor dx,dx
        jmp label1
    print1:
        cmp cx,0
        je salir
        pop dx
        add dx,48
        mov ah,02h
        int 21h
        dec cx
        jmp print1
    salir:
endm

;--------------------------------------------------- End macros menus -----------------------------------------------------
;mostrar top 10 GENERAL
TOP10Texto macro
    LOCAL iteracion,final
    clearScreen

    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    
    print msgtopdiezglobal
    
    printChar 10d;\n
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    printChar 32d ;espacio
    print msgpalabrausuario
    printchar 10d
    push si
    push di
    push ax
    push bx
    push cx
    push dx

    xor ax,ax
	xor di,di
	mov bx,1
    xor si,si
    inc si

	iteracion:
		cmp di,40d
		je final

		mov ax,ArrPunteoTodos[di];recuperar dato
        mov NTempOrdenamiento, ax
        dwtodb NTempOrdenamiento,numerotemporal
		
		;MODIFICAR NombreJugadorActual
		mov dx, ArrIndiceTodos[di]
		mov IndiceNombreTemp,dx
        ObtnerNombrePorIndice IndiceNombreTemp

        ;NombreJugardorActual


        ;----------------------------------------
        mov Numtemp,si
        
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        printChar 32d ;espacio
        dwtodb Numtemp,txtSalalida
        imrpimirtodo SIZEOF txtSalalida,txtSalalida; Correlativo
        printChar 46d ;pto
        printChar 32d ;Espacio
        imrpimirtodo SIZEOF numerotemporal,numerotemporal
        printchar 45d;guion
        printchar 45d
        printchar 45d
        printchar 45d
        printchar 45d
        printchar 45d
        printchar 45d
        printchar 45d
        ;printChar 47d ;/
        imrpimirtodo SIZEOF NombreJugardorActual,NombreJugardorActual
        printChar 10d ; \n
        inc si
		add di,4
		jmp iteracion
	final:
    call GetKey
    pop dx
    pop cx
    pop bx
    pop ax
    pop di
    pop si

endm

;Otro modo video
TOP10DW macro
    LOCAL iteracion,final
    push si
    push di
    push ax
    push bx
    push cx
    push dx

    xor ax,ax
	call cambiartodoavideo
	pintartexto NombreOrdenamiento,0d,0d,15d
	pintartexto Direccion,0d,14d,15d
	pintartexto textoVelocidad,0d,18d,15d
	pintartexto Velocidad,0d,26d,15d
	pintartexto TiempoTranscurrido,0d,32d,15d
	dibujarcuadro 0d,13d,350d,5d,200d

	xor di,di
	mov bx,1
	mov posiciontemporal,2d
	iteracion:
		cmp di,40d
		je final

		mov ax,ArrPunteoTodos[di];recuperar dato
       
        mov NTempOrdenamiento, ax
       
        add posiciontemporal,2d

        add ax,1d
		mov bx,3d ; multiplicador
		mul bx
        

        
        dwtodb NTempOrdenamiento,numerotemporal
		pintartexto numerotemporal,posiciontemporal,0d,15d
        dibujarcuadro 20d,posiciontemporalcualdro,ax,11d,140d

		;MODIFICAR NombreJugadorActual
		mov dx, ArrIndiceTodos[di]
		mov IndiceNombreTemp,dx
        ObtnerNombrePorIndice IndiceNombreTemp
		;

		pintartexto NombreJugardorActual,posiciontemporal,25d,15d
		mov ax,posiciontemporalcualdro
		add ax,010h
		mov posiciontemporalcualdro,ax
		add di,4
		jmp iteracion
	final:
		pintartexto metrica,24d,0d,15d
        mov  numerotemporalword, 10d
        mov posiciontemporalcualdro, 30d

    pop dx
    pop cx
    pop bx
    pop ax
    pop di
    pop si
endm

ObtnerNombrePorIndice macro indice
    LOCAL L1,L2,L3,L4,L5,L6,L7
    push si
    push di
    push ax
    push bx
    xor si,si

    LimpiarTodo SIZEOF NombreJugardorActual,NombreJugardorActual,0
    
    L1:
    cmp si,15d
    je L2

    mov ax,indice
    mov bx,37d
    mul bx
    add ax,si

    mov di,ax
    mov al,AllUsers[di]
    cmp al,"$"
    je L3
    mov NombreJugardorActual[si],al
    L3:
    inc si
    jmp L1
    L2:
    pop bx
    pop ax
    pop di
    pop si
endm

TOP10 macro

    xor ax,ax
	call cambiartodoavideo ;cambia todo al modo de video
	;pintartexto texto,fila,columna,color[89/100/115/50/59]
	pintartexto NombreOrdenamiento,0d,0d,15d ;mandar el nombre como parametro
	pintartexto Direccion,0d,14d,15d
	pintartexto textoVelocidad,0d,18d,15d
	pintartexto Velocidad,0d,26d,15d
	pintartexto TiempoTranscurrido,0d,32d,15d

	;[x,y,ancho,alto,color]
	dibujarcuadro 0d,13d,350d,5d,200d

	xor di,di
	mov bx,1
	mov posiciontemporal,2d
	iteracion:
		cmp di,014h
		je final
		xor ax,ax
		mov al,eltop[di]
		inc di
		mov ah,eltop[di]
		mov numerotemporal[0],al
		mov numerotemporal[1],ah
		add posiciontemporal,2d
		cadenaToSword2 numerotemporal,numerotemporalword
		mov ax,numerotemporalword
        ;PAnyNumber
        ;printChar 10d

		mov bx,3d
		mul bx
		;mov ax,20
		pintartexto numerotemporal,posiciontemporal,0d,15d;modificar el 4 -posicion
		
        dibujarcuadro 20d,posiciontemporalcualdro,ax,11d,140d;300 -largo, 36 posiccion
		;MODIFICAR NombreJugadorActual
		
		
		;
		pintartexto NombreJugardorActual,posiciontemporal,25d,15d
		mov ax,posiciontemporalcualdro
		add ax,010h
		mov posiciontemporalcualdro,ax
		inc di
		jmp iteracion
	final:
		pintartexto metrica,24d,0d,15d
		;pintartexto NombreJugardorActual,22,15,15
    
	getChar
endm

cadenaToSword2 macro texto,var	;Convierte una variable de tipo texto a una de tipo word
    LOCAL buclerecorrercadena,FinalC
   
    ;print msgE0
    ;print texto
    ;print msgSalto
    mov var,00h
    ;sizeCadena texto 
    mov si,2d
    dec si; esto decrementa para que no se pase el indice, el tamaño es mayor al indice
    xor ax,ax   ; ESTA ES LA VARIABLE TEMPORAL
    mov bx,1; inicio el multiplicador en 1
    
    buclerecorrercadena:;recorre la cadena desde laultima posicino hasta la primera    
        mov al,texto[si] ;temporal = entrada[i]
        sub ax,030h ; temporal = temporal - 48
        mul bx; multiplica el valor de la cadena en un espacio por el multiplicador y la coloca bx ; temporal = temporal * multiplicador
        add var, ax; binario+=bx    ; entero = enterio + temporal
        mov ax,0Ah ;incrementar el multiplicador en 10    
        mul bx
        mov bx,ax
        cmp si,00h
        je FinalC
        xor ax,ax
        dec si ; decrementa el contador de la variable    
        jmp buclerecorrercadena
    FinalC:
    
endm