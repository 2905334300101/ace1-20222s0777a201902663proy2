
;--------------------------------------------------- Macros Menus-----------------------------------------------------
print macro cadena	;imprime una cadena, imprime una variable hasta en contrar $
    mov ah, 09h
    mov dx, offset cadena
    int 21h
endm

printnum macro num	;imprime un numero de 8 bits
    mov ah,02h
    mov dl,num
    add dl,'0'
    int 21h
endm

getChar macro ;pide un char en teclado y lo guarda en al
    mov ah, 01h
    int 21h
endm

getPass macro ;CREO pide un caracter en consola pero no lo imprime
    mov ah,07h
    int 21h
endm

;Imprime un caracter
printChar8 macro char	;imprime un unico caracter de una variable byte
    mov ah,02h
    mov dl, char
    int 21h
endm

printChar16 macro var	;imprime un caracter en una variable word
    mov ah,02h
    mov dx, var
    int 21h
endm

getTexto macro array	;pide un texto y lo almacena en array
    LOCAL getCadena, finCadena
    mov si,0h    ;xor si,si

    getCadena:
        getChar
        cmp al,0dh
        je finCadena
        mov array[si],al
        inc si
        jmp getCadena
    finCadena:
    mov al,24h
    mov array[si],al
endm

getUsuario macro array	;pide un texto y lo almacena en array
    LOCAL getCadena, finCadena
    mov si,0h    ;xor si,si

    getCadena:
        getChar
        cmp al,0dh
        je finCadena
        mov array[si],al
        inc si
        jmp getCadena
    finCadena:
    ;mov al,24h
    ;mov array[si],al
endm

getPassword macro texto ;guarda en variable "texto" el texto ingresado colocado como $
    LOCAL getCadena, finCadena
    xor si,si
    getCadena:
        getPass
        cmp al,0dh ;compara si es retorno de carro
        je finCadena ;si la condicion se cumple finaliza la cadena
        mov texto[si],al ;almaceno byte a byte en la variable texto lo que esta en al
        inc si ;incremento el contador indice de texto

        mov ah,2h   ;imprime un caracter en consola
        mov dl,'$'      
        int 21h
        xor dl,dl

        jmp getCadena ;pido otro caracter
    finCadena:
        ;mov al,24h;coloco el sigo de $ en al 
        ;mov texto[si],al; muevo al a la posicon de texto
endm

sizeCadena macro cadena		;cuenta cuantos espacios tiene una cadena y guarda el resultado en al
    LOCAL buclecontarcadena,finalcadena
    xor si,si
    buclecontarcadena:
        cmp cadena[si],'$';compara si la cadena termino
        je finalcadena
        inc si
        jmp buclecontarcadena
    finalcadena:
    mov ax,si
endm

mFinalizarPrograma macro    
    ;SIN ESTAS LINEAS EL CODIGO NO TERMINA NUNCA Y NO REGRESA A LA TERMINAL
    xor al,al
    mov ah,4ch
    int 21h 
    ;mov al,10   ;respuesta en EXADECIMAL [10 es 16 en decimal]
    ;mov ah,4C   ;devuelve el control al sistema
    ;INTERRUPCION 21h -esto hace que el sistema se detenga y vea que hay en "ah" y en "al" y va a reaccionar dependiendo de los valores que estos tengan
endm

Getiguala macro char ;pide un caracter y lo compara al parametro y si es igual sale de la macro
    LOCAL is_enter ,exit
    is_enter:
        getPass
        cmp al,char[0]
        je exit
        jmp is_enter
    exit:
endm

;--------------------------------------------------- End macros menus -----------------------------------------------------