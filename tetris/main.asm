include macrosp.asm
include varios.asm
include macros.asm
.model small
;.stack 1000h
.stack
.data
;------------------------------------------ Parte de Menus -------------------------------------------------------------------------------------------
user db "OSCAR OLIVA"
;tablero db 100 dup(45),10,13,"$"
tablerotmp db 150 dup(45),"$"
;zpintar db "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*****-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-******","$"
piezasig db 16 dup(45),"$"

isespecial db 0b

valiarponer_sacar db 0b
valiarponer_sacar_sig db 0b
valsobreposicion db 0b
valrotacion db 0b


valnivel1 db 0b
valnivel2 db 0b

vallinea db 1b
vallineaSalir db 0b
;------------Piezas normales-----------------------
indexC dw 0d

posicionR dw 0d
posy1 dw 0d
posx1 dw 0d
posy2 dw 0d
posx2 dw 0d
posy3 dw 0d
posx3 dw 0d
posy4 dw 0d
posx4 dw 0d


posy1_l dw 0d
posx1_l dw 0d
posy2_l dw 0d
posx2_l dw 0d
posy3_l dw 0d
posx3_l dw 0d
posy4_l dw 0d
posx4_l dw 0d

posy1_Sig dw 0d
posx1_Sig  dw 0d
posy2_Sig  dw 0d
posx2_Sig  dw 0d
posy3_Sig  dw 0d
posx3_Sig  dw 0d
posy4_Sig  dw 0d
posx4_Sig  dw 0d

nivel dw 8d
;nivel dw 6d
nivelactual db "1"
siguientenivel db "2"
nivelactualnumero dw 1d
punteo dw 0d
contadorfilashechas dw 00
contadorpiezas dw 00

posyFinal dw 0d
prueba db 0
;-----------------VIDEO-------------------------
coord_x dw 0
coord_y dw 0
color_video db 0

video_x dw 0
video_y dw 0
indice_vid dw 0
temporal_video dw 0
contadormarco dw 0

;----------------- Generalidades--------------------

piezaactual db 0
randomNum db 9
randomNumSig db 9
randomPos db 0
RotacionNum db 0
contadorfecha db 0
pFin db "!!!GAME OVER!!!"

pRotar db "Rotar","$"
pDer db "Mov derecha","$"
pIzq db "Mov izquierda","$"


aimprimir db ?,"$"
imprimirsimbolodesalida db "$"
imprimirsimbolodesalida2 db "$"
;-------------------------Tiempo---------------------
ultimosegundoenpausa db 0
ultimosegundo db 0
ultimocentisegundo db 0

TIEMPOFINALIZADO db 6 dup("0")

literalminutoenpausa dw 0
stringminutoenpausa db 2 dup("0")

literalsegundoenpausa dw 0
stringsegundoenpausa db 2 dup("0")

literalminuto dw 0
stringminuto db 2 dup("0")

literalsegundo dw 0
stringsegundo db 2 dup("0")

literalcentisegundo dw 0
stringcentisegundo db 2 dup("0")


;textofinalizar db "$"
;identificacion db "Universidad de San Carlos de Guatemala",10,13,"Facultad de Ingenieria",10,13,"Escuela de Ciencias y Sistemas",10,13,"Arquitectura de Computadores y Ensambladores",10,13,"Sección A","$" 
nombrecreador db "Oscar Daniel Oliva España",10,13,"$" 
carnetcreador db "201902663",10,13,"$"

pausaenpausa db "----PAUSA----"
pausaenpausados db "SUPRIMIR = Regresar al juego"
pausaenpausatres db" ESC = Finalizar Juego"

msgsubionivel db "------SUBIO DE NIVEL------"
msgsubioniveluno db "Barra Espaciadora = Continuar"
msgempezandonivel db "   EMPEZANDO NIVEL:   "
msgpuntajeactual db "   PUNTAJE ACTUAL:   "
msgtiempoactual db "   TIEMPO ACTUAL:   "
msgpalabranivel db "NIVEL:"
msgpalabrapuntaje db "PUNTAJE:"
msgpalabratiempo db "TIEMPO:"
msgpalabratiempoenpausa db "TIEMPO EN PAUSA:"
msgpalabracentisegundo db "CENTI:"
msgdospuntos db ":"
msgganador db "!!!FELICIDADES!!!"
msgganadoruno db "GANASTE TETRIS"
msgganadordos db "Barra Espaciadora = Ir al menu"
msgpalabrausuario db "PUNTEO -------- USUARIO$"
separadores db "--------$"


;------------------------------------------ Parte de Menus -------------------------------------------------------------------------------------------

;------Menus
varespacio db "$"
msgdatosdelcreador db "    ---------------------------DATOS DEL CREADOR----------------------------$"
enterespaciovar db "     ----------------------Presione ENTER para continuar---------------------","$" 
identificacion db "          -       Universidad de San Carlos de Guatemala     -",0ah,"          -               Facultad de Ingeniería            -",0ah,"          -          Escuela se Ciencias y Sistemas          -",0ah,"          -   Arquitectura de Compiladores y Ensambladores 1 -",0ah,"          -                     Sección A                   -",0ah,"          -            Oscar Daniel Oliva España            -",0ah,"          -                    201902663                     -$"
InstruccionesMenuPrincipal db " Presione una tecla para continuar ","$"
varlineastexto db "    ---------------------------------------------------------------$"
MenuPrincipal db "        ---------------------MENÚ PRINCIPAL---------------------",0ah,0ah,"                             F1  = Login",0ah,"                             F5  = Registrar",0ah,"                             F9  = Salir",0ah,"$"
espaciomenuprincipal db "        --------------------------------------------------------$"
MenuUsuarioString db 10,13,"        ---------------------Menú de Usuario---------------------",0ah,"                             F3  = Jugar",0ah,"                             F4  = Top 10 general",0ah,"                             F5  = Top 10 personal",0ah,"                             F10 = Cerrar sesion",0ah,"$"
MenuAdminString  db 10,13,"        ---------------------Menú de Aministrador Maestro---------------------",0ah
MenuAdminString2 db 10,13,"                             F1  = Desbloquear Usuario",0ah,"                             F2  = Promover ususario",0ah,"                             F3  = Degradar usuario",0ah,"                             F4  = Bubble Sort",0ah, "                             F5  = Heap Sort",0ah,"                             F6  = Quick Sort",0ah,"                             F10 = Cerrar sesion",0ah,"$"
MenuAdminStringv2  db 10,13,"        ---------------------Menú Administrador---------------------",0ah
MenuAdminStringv22 db 10,13,"                        F1  = Desbloquear Usuario",0ah,"                        F2  = Mostrar el top 10 general de puntuaciones",0ah,"                        F3  = Mostrar el top 10 de las puntuaciones del jugador",0ah,"                        F4  = Bubble Sort",0ah, "                        F5  = Heap Sort",0ah,"                        F6  = Quick Sort",0ah,"                        F7  = Jugar",0ah,"                        F10 = Cerrar sesion",0ah,"$"

EnterParaContinuar db 0ah,0ah,"Presione ENTER para continuar$"
FinPrograma db 0ah," Cerrando Programa, WORALE $"
userbloqueado db 10,13,10,13,"            El Usuario se encuentra bloqueado",10,13, "       Debe de esperar a que un usuario admin lo desbloquee$"
useradminbloqueado db 10,13,"         El Usuario Administrador está bloqueado",10,13,"      Espere 30 segundos para volver a ingresar las credenciales$"
useradmindesbloqueado db 10,13,"      Se han cumplido los 30 segundos de bloqueo",10,13,"       Puede volver a ingresar sus credenciales$"

;-------Base de Datos
AllUsers db 1850d dup('$') ;37 bytes por usuario y contraseña, 50 usuarios 
;[15bytes Usuario][20bytes Password][1byte Intentos][1byte Administador ?]
;0000 0000 -> byte -> char

AllPunteos db 1000d dup('0') ;2 bytes por numero x 10 numeroa a guardar 20, 20 x 50 urs = 10000
UsuarioActual db 00h

;------------Login------------
IngreseLogUsuario db "             Ingrese el nombre de usuario           $"
LogUsuario db "      Usuario: $"
IngreseLogPassword db "              Ingrese la contraseña               $"
LogPassword db "      Contraseña: $"
Username db 15 dup('$')
Password db 20 dup('$')
Usernameausar db 15 dup(0)

;----------Registro----------
RUsuario db 15 dup('$')
RPassword db 20 dup('$')
TipoUsuario db "-$"

UsuarioLimpio db 15 dup('$')
PasswordLimpio db 20 dup('$')

pnoFunciona db "Bienvenido al sistema$" 

printRnum db "Mostrondo todo el arreglo$" 
input db 20 dup(' ')

;--------- Datos buscados
punteroiniciaUser dw 0d
seencontrUsuario db 0b
nombreUserBuscado db 15 dup('$'),"$"
NoBloqueoBuscado db "-$"
TipoBuscado db "-$"
punteoUsuarioBuscado db 20 dup('0'),"$"
PunteoTemporal dw 0d
CadenaPunteoTemp db "--$"
ArrPunteo dw 44 dup(0)
IndiceTemp dw 0d
incextemp dw 0d
punteoAEscrbir db 20 dup('0'),"$"

;-----debug
charenter db 0dh
contador1 dw  0d
Numtemp dw  0d
txtSalalida db 2 dup(0)

;------Mensajes del admin
printmsgPromover db  "                    Ingrese el usuario que desea promover","$"
printPromovido db "                    Se promovió correctamente al usuario","$"
printNoPromovido db "                    El usuario ya no se puede promover más","$"

printmsgDegradar db  "                    Ingrese el usuario que desea degradar","$"
printDegradado db "                    Se ha degradado correctamente al usuario","$"
printNoDegradado db "                    El usuario ya no se puede degradar más","$"

printmsgDesbloquead db  "                    Ingrese el usuario que desea desbloquear",10,13,"$"
printBlqoueado db "                    Se ha desbloqueado al usuario con éxito$"
printNoBlqoueado db "                    El usuario no se encuentra bloqueado$"

;-------------Mensajes USuario
printTop10 db  "                TOP 10 PERSONAL$"
msgtopdiezglobal db "TOP 10 GLOBAL$"

;------print puenteos
punteo1 db "1.$"

;------ Pruebas
p0 db "Prueba0","$"
p1 db "prueba 1$"
p2 db "prueba 2$"
p3 db "prueba 3$"
p4 db "prueba 4$"
p5 db "prueba 5$"
p6 db "prueba 6$"
p7 db "prueba 7$"
p8 db "prueba 8$"
p9 db "prueba 9$"
p10 db "prueba 10$"
p11 db "prueba 11$"
p12 db "prueba 12$"

;----- Mensajes varios
plog db 10,13,"                    Credenciales correctamente ingresadas$" 
ErrorPasswordCorta db "la contraseña tiene que tener minimo 16 caracteres$"
ErrorPasswordLarga db "la contraseña tiene que ser maximo de 20 caracteres$"
ErrorPasswordNoCorrecta db 10,13,"                    La contraseña no es correcta$"
ErrorUsuarioNoExistente db 10,13,"                    El Usuario ingresado no existe$"
ErrorNumMayus db 10,13,"                    La contraseña debe tener al menos 3 mayusculas$"
ErrorNumNum db 10,13,"                    La contraseña debe tener al menos 2 numeros$"
ErrorNumEspecialesPass db 10,13,"                    La contraseña debe tener al menos 2 caracteres especiales @,>,+,*,#,@ $"
ErrorNoNumber db 10,13,"                    El usuario no puede empezar por un numero$"
ErrorNoSimbol db 10,13,"                    Caracter no valido: $"
ErrorTamanoUsarioMenor db 10,13,"                    La longitud del usuario es menor a 8 caracteres$"
ErrorTamanoUsarioMayor db 10,13,"                    La longitud del usuario es mayor a 15 caracteres$"
ErrorUsuaioExsitente db 10,13,"                    El usuario ya existe$"
MsgUsuarioAgregado db 10,13,"                    Usuario agregado correctamente $"
Tregistro db 10,13,"                    Registro de Usuario",0ah,"$"
Felicidades db 10,13,"         Todos los valores son aceptados, se procederá a crear al usuario$"
ImprimirAllUsuarios db 10,13,"                    1. imprimir memoria de usuarios y contrasenias$"
rutaUsers db "users.tet",0
rutaPunteos db "punt.tet",0
textoABC db ?,"$"
hanldetemp dw 0d

; ------Pruebas
zzzz db "serchiboi$$$$$$"
zzzzPunteos dw 20d
tt db "termino$"


; ordenameintos 
ArrPunteoTodos dw 2000 dup(0)
ArrIndiceTodos dw 2000 dup(0)
velocidadOrdenamiento dw 2d
NTempOrdenamiento dw 0d
IndiceNombreTemp dw 0d
;--------------------------------------------------------------Termina parte de Menus ------------------------------------------------


;------------------------------------------Otro modo video--------------------------------------------------------------------------------
eltop db "11325632454323456799"
NombreOrdenamiento db "ordenamiento"
Direccion db "->"
textoVelocidad db "Speed:"
Velocidad db "5"
TiempoTranscurrido db "00:00:00"
NombreJugardorActual db "AlexisMarcoTula"

numerotemporal db "$$"
numerotemporalword dw 10
posiciontemporal db "0"
posiciontemporalcualdro dw 30
metrica db "score"

;--------------------------------------------------------------Termina parte de Menus ------------------------------------------------


.code

main proc
    mov dx, @DATA   
    mov ds , dx     
    ;-----Ejecutar esto la primera vez luego cerrar y comentar
    ;crearF rutaUsers
    ;crearF rutaPunteos
    ;getChar
    ;-----------------------------------------------------


    ;editarF rutaUsers,AllUsers
    ;editarF rutaPunteos,AllPunteos
    ;getChar
    ;println nombreUserBuscado
    ;getChar

    abrirF rutaUsers,hanldetemp
    leerF SIZEOF AllUsers, AllUsers, hanldetemp
    ;imrpimirtodo SIZEOF AllUsers,AllUsers
    ;getChar

    abrirF rutaPunteos,hanldetemp
    leerF SIZEOF AllPunteos, AllPunteos, hanldetemp
    ;imrpimirtodo SIZEOF AllPunteos,AllPunteos
    ;getChar

    ;limparvarslogin
    ;getUsuario Username
    ;AgregarPunteo zzzzPunteos
    ;getChar

    clearScreen
    println varespacio
    println msgdatosdelcreador
    println varespacio
    println identificacion
    menuInicio:
    println varespacio
    print enterespaciovar
    limpearVariablesRegistro
    limpearVariablesLogin
    Getiguala charenter ;verifica si la tecla ingresada es un enter, no sale de aqui, posible ciclado
    clearScreen
    println varespacio
    println MenuPrincipal
    println espaciomenuprincipal
    getoptionmenuprincipal:
        call GetKey
        ;getPass
        cmp ax,3b00h
        je LOGIN
        cmp ax,3f00h
        je REGISTROUSUARIO
        cmp ax,4300h
        je exitPrograma
        cmp ax,0231h
        je MENUDEBUG
        jmp getoptionmenuprincipal

    MENUDEBUG:
    clearScreen
    print ImprimirAllUsuarios
    getPass
    cmp al,'1'
    je ImprimirMemoriaUsuairiosYcontrasenias
    jmp menuInicio


    ImprimirMemoriaUsuairiosYcontrasenias:
        clearScreen
        xor si,si
        printAllChar:
        printChar8 AllUsers[si]
        inc si
        cmp si,715h;73A /| 49 usuarios
        je menuInicio
        jmp printAllChar



    LOGIN:
        clearScreen
        limpearVariablesRegistro
        limpearVariablesLogin
        limparvarslogin
        println IngreseLogUsuario
        println espaciomenuprincipal
        print LogUsuario
        getUsuario Username
        println IngreseLogPassword
        println espaciomenuprincipal
        print LogPassword
        getPassword Password
        LoginFase1

    REGISTROUSUARIO:
        ValidarRegistro
        AgregarNuevoUsuario
    
    exitPrograma:
        print FinPrograma
        mFinalizarPrograma

    ;====
    
main endp

GetKey proc
    xor ah, ah
    int 16h
    ret
GetKey endp

HasKey proc
    push ax
    mov ah, 00001h
    int 16h
    pop ax
    ret 
HasKey endp

VSync proc
    mov dx, 03dah
    WaitNotVSync:
        in al, dx
        and al, 08h
        jnz WaitNotVSync
    WaitVSync:
        in al, dx
        and al, 08h
        jz WaitVSync
    ret
VSync endp

cambiartodoavideo proc
    iniciarmodovideo
    cambiaravideo
    entraramodovideo
    ret
cambiartodoavideo endp

PlayGame proc
    LimpiarTodo 150,tablerotmp,45d
    mov literalminuto,0
    mov literalminutoenpausa,0
    mov literalsegundo,0
    mov literalsegundoenpausa,0
    mov literalcentisegundo,0
    mov ultimocentisegundo,0
    mov ultimosegundo,0
    mov ultimosegundoenpausa,0
    mov ultimosegundoenpausa,0
    mov punteo,0
    mov valnivel1,0b
    mov valnivel2,0b
    mov vallinea,1b
    mov vallineaSalir,0b
    mov indexC,0d
    mov nivelactualnumero,1d
    mov nivel,7d
    ;mov nivel,2d
    mov valiarponer_sacar,0b
    mov valiarponer_sacar_sig,0b
    mov valsobreposicion,0b
    mov valrotacion,0b
    pasardatosastring
    call cambiartodoavideo  
    xor si,si

    Iniciar:
    calcularsegundo
    generarpieza
    FinJuego

    L1:
    pintartexto Usernameausar,1,6,15
    xor si,si
        
    cambiarnivel
    mov prueba, 0
    ;push ax
    mov ax, punteo

    ;PAnyNumber
    ;pop ax
    validacionponerpieza
    
    call ImprimirConPieza

    Lineas
    Lineas
    Lineas
    Lineas
    ;movimiento
    movabajo
    call VSync
    call VSync
    call delayGame
    call VSync
    call VSync

    MAS:
    call HasKey

    jz L1
    xor ax,ax
    call GetKey ;verificar teclas
    cmp ax, 5300h;suprimir
    je L2
    cmp ax, 4b00h;flecha izquierda
    je moverizq
    cmp ax, 4d00h;flecha derecha
    je moverder
    cmp ax,  3920h ;espacio
    je rotar 
    cmp ax,011Bh;ESC
    je pausa
   
    moverizq:
    ;println pIzq
    movaIzq
    call ImprimirConPieza
    inc prueba
    cmp prueba,4
    je L1
    jmp MAS

    moverder:
    ;println pDer
    movaDER
    call ImprimirConPieza
    inc prueba
    cmp prueba,4
    je L1
    jmp MAS

    rotar:
    ;println pRotar
    rotarpieza
    call ImprimirConPieza
    inc prueba
    cmp prueba,4
    je L1
    jmp MAS

    pausa:
    imprimirenvideo
    pintartexto pausaenpausa,7,13,50
    pintartexto pausaenpausados,10,6,110
    pintartexto pausaenpausatres,12,8,110
    pintartexto msgpalabratiempoenpausa,22,3,150
    pintartexto msgdospuntos,22,24,100

    pintartexto msgpalabranivel,15,12,150
    Num2Str nivelactualnumero,aimprimir
    pintartexto aimprimir,15,29,150

    pintartexto msgpuntajeactual,18,8,150
    Num2Str punteo,aimprimir
    pintartexto aimprimir,18,29,150
    pausa3:
    calcularsegundopausa
    Num2str literalminutoenpausa,stringminutoenpausa
    pintartexto stringminutoenpausa,22,20,150
    Num2str literalsegundoenpausa,stringsegundoenpausa
    pintartexto stringsegundoenpausa,22,30,150

    call HasKey
    jz pausa3


    pausa2:
    call GetKey
    cmp ax,5300h;SUPR = REGRESAR
    je videopausa
    cmp ax,011Bh ;ESC = TERMINAR JUEGO
    je L2
    jmp pausa3
    videopausa:
    imprimirenvideo
    mov literalsegundoenpausa,0
    mov literalminutoenpausa,0
    jmp L1

    LFinJuego:
    
    pintartexto pFin,4,12,46
    call GetKey

    L2:
    regresaramodotexto
    println p8
    ;println p2
   
    ;SALIR
    ret
    ;mov ah,4ch
    ;xor al,al
    ;int 21H
PlayGame endp

ImprimirConPieza proc
	;escribirlo
    ponerpieza
    
    ;mostar tablero
    mostrartaTempb
   
    ;quitarlo
    mov valiarponer_sacar,1b
    ponerpieza
    mov valiarponer_sacar,0b
    ret
ImprimirConPieza endp

generateRandomNumber proc
   
    call delay

    mov ah,0h
    int 1ah

    mov ax,dx
    mov dx,0
    mov bx,8
    div bx
    mov randomNum,dl
    ret
generateRandomNumber endp

generateRandomNumberSig proc
   
    call delay

    mov ah,0h
    int 1ah

    mov ax,dx
    mov dx,0
    mov bx,8
    div bx
    mov randomNumSig,dl
    ret
generateRandomNumberSig endp

pedirteclaproc proc
    xor ah,ah
    int 16h
    ret
pedirteclaproc endp

delay proc
        mov cx,1
    startDelay:
        cmp cx,30000
        je endDelay
        inc cx
        jmp startDelay
    endDelay:
        ret
delay endp

delay2 proc
        push si
        xor si,si

    initdelay:
        mov cx,1
    startDelay2:
        cmp cx,90000h
        je endDelay2
        inc cx
        jmp startDelay2
    endDelay2:
        inc si
        cmp si,5
        jne initdelay
        pop si
        ret
delay2 endp

delayGame proc
        push si
        xor si,si
    initdelay:
        mov cx,1
    startDelay2:
        cmp cx,90000h
        je endDelay2
        inc cx
        jmp startDelay2
    endDelay2:
        inc si
        cmp si,nivel
        jne initdelay
        pop si
        ret
delayGame endp

delay3 proc
        push si
        xor si,si

    initdelay:
        mov cx,1
    startDelay2:
        cmp cx,90000h
        je endDelay2
        inc cx
        jmp startDelay2
    endDelay2:
        inc si
        cmp si,10
        jne initdelay
        pop si
        ret
delay3 endp

delay4 proc
    push si
    xor si,si

    initdelay:
        mov cx,1
    startDelay2:
        cmp cx,90000h
        je endDelay2
        inc cx
        jmp startDelay2
    endDelay2:
        inc si
        cmp si,15
        jne initdelay
        pop si
        ret
delay4 endp

end main

