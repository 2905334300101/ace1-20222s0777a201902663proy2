;------------------------------Juego----------------------------------------
cambiarnivel macro
	LOCAL L1,L2,L3,L4,L5,L6,L7,L8
	cmp punteo,10d
	jae L7
	cmp punteo,5d
	jae L2
	cmp punteo,2d
	jae L1
	jmp L4

	L2:
		CMP valnivel1,0b
		je L5
		jmp L4
	L5:
		mov nivelactualnumero,3d
		imprimirenvideo
		pintartexto msgsubionivel,5,5,50
		pintartexto msgsubioniveluno,9,3,50
		pintartexto msgempezandonivel,12,8,150
		Num2Str nivelactualnumero,aimprimir
		pintartexto aimprimir,12,29,100

		pintartexto msgpuntajeactual,14,8,150
		Num2Str punteo,aimprimir
		pintartexto aimprimir,14,29,100
		
		pintartexto msgtiempoactual,16,8,150
		Num2Str literalminuto,stringminuto
		pintartexto stringminuto,16,29,110
		pintartexto msgdospuntos,16,31,150
		Num2Str literalsegundo,stringsegundo
		pintartexto stringsegundo,16,33,110


		;println msgsubionivel
		
		;pausa
		call GetKey
		cmp ax, 3920h 
		jne L5

		mov valnivel1,1b
		push si
		;mov nivelactual[si],"2"
		pop si
		mov nivel,1d
		imprimirenvideo
		jmp L4

	L1:
		CMP valnivel2,0b
		je L6
		jmp L4
	L6:
		mov nivelactualnumero,2d
		imprimirenvideo
		pintartexto msgsubionivel,5,5,50
		pintartexto msgsubioniveluno,9,3,50
		pintartexto msgempezandonivel,12,8,150
		Num2Str nivelactualnumero,aimprimir
		pintartexto aimprimir,12,29,100

		pintartexto msgpuntajeactual,14,8,150
		Num2Str punteo,aimprimir
		pintartexto aimprimir,14,29,100
		
		pintartexto msgtiempoactual,16,8,150
		Num2Str literalminuto,stringminuto
		pintartexto stringminuto,16,29,100
		pintartexto msgdospuntos,16,31,110
		Num2Str literalsegundo,stringsegundo
		pintartexto stringsegundo,16,33,100
		
		;println msgsubionivel
		;pausa
		call GetKey
		cmp ax, 3920h 
		jne L6
		mov valnivel2,1b
		mov nivel,4d
		imprimirenvideo
		jmp L4

	L7:
		;call cambiartodoavideo
		imprimirenvideo

		pintartexto msgganador,7,10,100
		pintartexto msgganadoruno,10,11,100
		pintartexto msgganadordos,13,6,100
		call GetKey
		cmp ax,3920h
		jmp LFinJuego
	L4:
endm

Lineas macro
    LOCAL Leetab,Imprimirtab,Salirmostrartab,v1,v2,v3,v4,v5,v6,v7,NIVEL1,NIVEL2,NIVEL3,CONTINUAR

	;println tablerotmp
	;mov ah,4ch
    ;xor al,al
    ;int 21H
	mov vallinea,1b
    mov si,149
    mov di,00
    Leetab:
        mov al, tablerotmp[si]
		cmp al, "-"
		je v1
		jmp v2

		v1:
		mov vallinea,0b
		mov vallineaSalir,1b

		v2:
		;printChar al
        dec si
        inc di
        cmp di,10
        je Imprimirtab
        jne Leetab

    Imprimirtab:
		cmp vallinea,1b
		je v3
		jmp v4
		
		v3:
			mov di,si
			push si
			mov si,di
			add si,10d

			v5:
			mov vallineaSalir,0b
			bajarcolumna
			dec si
			cmp di,si
			je v6
			jmp v5

			v6:
			cmp nivelactualnumero,1d
			je NIVEL1
			cmp nivelactualnumero,2d
			je NIVEL2
			cmp nivelactualnumero,3d
			je NIVEL3

			NIVEL1:
			add punteo,1d
			jmp CONTINUAR
			NIVEL2:
			add punteo,1d
			add punteo,1d
			jmp CONTINUAR
			NIVEL3:
			add punteo,1d
			add punteo,1d
			add punteo,1d
			CONTINUAR:
			pop si
		v4:
			mov vallinea,1b
			xor di,di
			cmp si,39
			je Salirmostrartab
			jmp Leetab
    Salirmostrartab:
endm 

bajarcolumna macro
	LOCAL L1,L2
	push di
	mov di,si
	push si
	mov si,di
	sub di,10d
	L1:
		MOV al,tablerotmp[di]
		mov tablerotmp[si],al
		sub di,10d
		sub si,10d
		cmp si,39
		jbe L2
		jmp L1
	L2:

	pop si
	pop di

endm

rotarpieza macro
	LOCAL L1,L2,L3,L4,L5,L6,L7,LSALIR,Regresar,SALIRTOTAL
	
	push ax

	mov ax,posy1
	mov posy1_l ,ax

	mov ax,posx1
    mov posx1_l ,ax

	mov ax,posy2
    mov posy2_l ,ax

	mov ax,posx2
    mov posx2_l ,ax

	mov ax,posy3
    mov posy3_l ,ax

	mov ax,posx3
    mov posx3_l ,ax

	mov ax,posy4
    mov posy4_l ,ax

	mov ax,posx4
    mov posx4_l ,ax

	pop ax

	;caso 0 T
	cmp randomNum,0
	je L1
	cmp randomNum,1
	je L2
	cmp randomNum,2
	je L3
	cmp randomNum,3
	je L4
	cmp randomNum,4
	je L5
	cmp randomNum,5
	je L6
	cmp randomNum,6
	je L7
	jmp LSALIR

	L1:
	rotar_t
	jmp LSALIR

	L2:
	rotar_Linea
	jmp LSALIR

	L3:
	rotar_Lizq
	jmp LSALIR

	L4:
	rotar_Lder
	jmp LSALIR

	L5:
	jmp LSALIR
	L6:
	rotar_Zizq
	jmp LSALIR

	L7:
	rotar_Zder

	LSALIR:
	mov valrotacion,0b
	validarsobreposicion
	push ax

	mov al,valsobreposicion
	mov valrotacion,al

	pop ax
	
	validarposrotacion
	cmp valrotacion,1b
	jne SALIRTOTAL

	Regresar:
	push ax

	mov ax,posy1_l
	mov posy1,ax

	mov ax,posx1_l
    mov posx1,ax

	mov ax,posy2_l
    mov posy2,ax

	mov ax,posx2_l
    mov posx2,ax

	mov ax,posy3_l
    mov posy3 ,ax

	mov ax,posx3_l
    mov posx3,ax

	mov ax,posy4_l
    mov posy4,ax

	mov ax,posx4_l
    mov posx4,ax

	pop ax

	SALIRTOTAL:


endm

rotar_t macro
	LOCAL L1,L2,L3,L4,L5

	;mov posy1,2d;arriba
    ;mov posx1,1d

	cmp RotacionNum,0
	je L1
	cmp RotacionNum,1
	je L2
	cmp RotacionNum,2
	je L3
	cmp RotacionNum,3
	je L4

	L1:
		
		sub posy2,2d
    	;mov posx2,0d;mas izq

    	sub posy3,1d
    	sub posx3,1d;enmedio abajo

    	;mov posy4,3d
    	sub posx4,2d;mas der
		mov RotacionNum,1d

		jmp L5
	L2:
		
		;sub posy2,2d
    	add posx2,2d;mas izq

    	sub posy3,1d
    	add posx3,1d;enmedio abajo

    	sub posy4,2d
    	;sub posx4,2d;mas der

		mov RotacionNum,2d
		jmp L5
	L3:
		

		add posy2,2d
    	;mov posx2,2d;mas izq

    	add posy3,1d
    	add posx3,1d;enmedio abajo

    	;sub posy4,2d
    	add posx4,2d;mas der

		mov RotacionNum,3d
		jmp L5
	L4:
		
		;add posy2,2d
    	sub posx2,2d;mas izq

    	add posy3,1d
    	sub posx3,1d;enmedio abajo

    	add posy4,2d
    	;add posx4,2d;mas der

		mov RotacionNum,0d

	L5:

endm

rotar_Linea macro
	LOCAL L1,L2,L3

	;mov posy3,2d
    ;mov posx3,1d

	cmp RotacionNum,0
	je L1
	cmp RotacionNum,1
	je L2

	L1:
		add posy1,2d
		add posx1,2d;arriba

		add posy2,1d
		add posx2,1d

		sub posy4,1d
		sub posx4,1d;abajo

		mov RotacionNum,1
		
		jmp L3
	L2:
		sub posy1,2d
		sub posx1,2d;arriba
		
		sub posy2,1d
		sub posx2,1d

		add posy4,1d
		add posx4,1d;abajo
		
		mov RotacionNum,0

	L3:

endm

rotar_Lizq macro
	LOCAL L1,L2,L3,L4,L5

    ;mov posy3,2d
    ;mov posx3,6d;enmedio

	cmp RotacionNum,0
	je L1
	cmp RotacionNum,1
	je L2
	cmp RotacionNum,2
	je L3
	cmp RotacionNum,3
	je L4

	L1:
		;add posy1,1d
		add posx1,2d;iz

		add posy2,1d
		add posx2,1d;arriba

		sub posy4,1d
    	sub posx4,1d;abajo

		mov RotacionNum,1d
		jmp L5
	L2:
		add posy1,2d
		;mov posx1,5d;iz

		add posy2,1d
		sub posx2,1d;arriba

		sub posy4,1d
    	add posx4,1d;abajo


		mov RotacionNum,2d
		jmp L5
	L3:
		;mov posy1,1d
		sub posx1,2d;iz

		sub posy2,1d
		sub posx2,1d;arriba

		add posy4,1d
    	add posx4,1d;abajo
	

		mov RotacionNum,3d
		jmp L5
	L4:
		sub posy1,2d
		;mov posx1,5d;iz

		sub posy2,1d
		add posx2,1d;arriba

		add posy4,1d
    	sub posx4,1d;abajo


		mov RotacionNum,0d

	L5:


endm

rotar_Lder macro
	LOCAL L1,L2,L3,L4,L5

	

   ; mov posy3,2d
   ; mov posx3,6d;enmedio

   

	cmp RotacionNum,0
	je L1
	cmp RotacionNum,1
	je L2
	cmp RotacionNum,2
	je L3
	cmp RotacionNum,3
	je L4

	L1:
		add posy1,2d
    	;mov posx1,7d;der

    	add posy2,1d
    	add posx2,1d;arriba

	 	sub posy4,1d;abajo
    	sub posx4,1d


		mov RotacionNum,1d
		jmp L5
	L2:
		;add posy1,2d
    	sub posx1,2d;der

    	add posy2,1d
    	sub posx2,1d;arriba

	 	sub posy4,1d;abajo
    	add posx4,1d

		mov RotacionNum,2d
		jmp L5
	L3:
		sub posy1,2d
    	;mov posx1,7d;der

    	sub posy2,1d
    	sub posx2,1d;arriba

	 	add posy4,1d;abajo
    	add posx4,1d
	
		mov RotacionNum,3d
		jmp L5
	L4:

		;sub posy1,2d
    	add posx1,2d;der

    	sub posy2,1d
    	add posx2,1d;arriba

	 	add posy4,1d;abajo
    	sub posx4,1d
		
		mov RotacionNum,0d

	L5:


endm

rotar_Zizq macro
	LOCAL L1,L2,L3

    ;mov posy2,2d
    ;mov posx2,7d;der arriba

	cmp RotacionNum,0
	je L1
	cmp RotacionNum,1
	je L2

	L1:
		sub posy1,1d
		add posx1,1d;iz arriba

		sub posy3,1d
		sub posx3,1d;iz abajo

		;mov posy4,3d
		sub posx4,2d;der abajo

		mov RotacionNum,1
		
		jmp L3
	L2:
		add posy1,1d
		sub posx1,1d;iz arriba

		add posy3,1d
		add posx3,1d;iz abajo

		;mov posy4,3d
		add posx4,2d;der abajo
		
		mov RotacionNum,0

	L3:
	
endm

rotar_Zder macro
	LOCAL L1,L2,L3

    ;mov posy3,2d
    ;mov posx3,4d ;iz arriba

	cmp RotacionNum,0
	je L1
	cmp RotacionNum,1
	je L2

	L1:
		sub posy1,2d
    	;mov posx1,3d ;iz abajo

		sub posy2,1d
		sub posx2,1d ;der abajo

		add posy4,1d
		sub posx4,1d ;der arriba

		mov RotacionNum,1
		
		jmp L3
	L2:
		add posy1,2d
    	;mov posx1,3d ;iz abajo

		add posy2,1d
		add posx2,1d ;der abajo

		sub posy4,1d
		add posx4,1d ;der arriba
		
		mov RotacionNum,0

	L3:
endm

validarposrotacion macro
	LOCAL SALIR
	validarposrotacion_esp posx1,posy1
	cmp valrotacion,1b
	je SALIR
	validarposrotacion_esp posx2,posy2
	cmp  valrotacion,1b
	je SALIR
	validarposrotacion_esp posx3,posy3
	cmp valrotacion,1b
	je SALIR
	validarposrotacion_esp posx4,posy4
	
	SALIR:
endm

validarposrotacion_esp macro posx,posy
	LOCAL L1X,L2X,L3X,L4X,L5X,L6X,L7X,L8X,L9X,L10X,LEX

	cmp posx,0d
	jae L1X
	jmp LEX

	L1X:
	cmp posx,9d
	jbe L2X
	jmp LEX

	L2X:
	cmp posy,14
	JBE L3X
	JMP LEX

	L3X:
	cmp posy,3
	JAE L4X

	LEX:
	mov valrotacion,1b
	
	L4X:
	
endm

validacionponerpieza macro
	LOCAL LSALIR
	
	validacionposicion
	
	jmp LSALIR

	plasmarpieza:
	ponerpieza
	mostrartaTempb
	jmp Iniciar

	LSALIR:

endm

validacionposicion macro
	LOCAL COLOCAR,LSALIR

	cmp posy1,14d
	je COLOCAR

	cmp posy2,14d
	je COLOCAR

	cmp posy3,14d
	je COLOCAR

	cmp posy4,14d
	je COLOCAR
	jmp LSALIR


	COLOCAR:
	jmp plasmarpieza
	
	LSALIR:

endm

validarsobreposicion macro
	LOCAL LSALIR
	validarposicion posy1,posx1
	cmp valsobreposicion,1b
	je LSALIR
	validarposicion posy2,posx2
	cmp valsobreposicion,1b
	je LSALIR
	validarposicion posy3,posx3
	cmp valsobreposicion,1b
	je LSALIR
	validarposicion posy4,posx4

	LSALIR:
endm

validarposicion macro posy,posx
	LOCAL L1,LSALIR
	calcrelativo posy,posx
	mov al, tablerotmp[si]
	cmp al,"-"
	jne L1
	jmp LSALIR
	L1:
	mov valsobreposicion,1b
	LSALIR:
endm

generarpieza macro
	LOCAL L1,L2,L3,L4,L5,L6,L7,L8,L9,L10,L11,L12,L13,L14,L1_sig,L2_sig,L3_sig,L4_sig,L5_sig,L6_sig,L7_sig,L8_sig,L9_sig,L10_sig,L11_sig,LSALIR

	cmp randomNum,9d
	je L9
	jmp L10
	L9:
		call generateRandomNumber
		call generateRandomNumberSig
		jmp L14
	L10:
		push ax
		mov al, randomNumSig
		mov randomNum,al
		call generateRandomNumberSig
		pop ax
	;mov randomNum,2
	;mov randomNumSig,2
	;mov ax,randomNum
	;PAnyNumber
	;call delay3
	L14:
	cmp contadorpiezas,5d
	jae L13
	jmp L11
	L13:
	mov randomNum,1
	mov contadorpiezas,1d


	L11:
	mov RotacionNum,0
	;caso 0 T
	cmp randomNum,0
	je L1
	cmp randomNum,1
	je L2
	cmp randomNum,2
	je L3
	cmp randomNum,3
	je L4
	cmp randomNum,4
	je L5
	cmp randomNum,5
	je L6
	cmp randomNum,6
	je L7
	cmp randomNum,7
	je L8
	jmp L11_sig
	L1:
	incializalizar_t
	jmp L11_sig
	L2:
	incializalizar_Linea
	jmp L11_sig
	L3:
	incializalizar_Lizq
	jmp L11_sig
	L4:
	incializalizar_Lder
	jmp L11_sig
	L5:
	incializalizar_cuadrado
	jmp L11_sig
	L6:
	incializalizar_Zizq
	jmp L11_sig
	L7:
	incializalizar_Zder
	jmp L11_sig
	L8:	
	incializalizar_pto

	;-------------------------------------
	L11_sig:
	cmp randomNumSig,0
	je L1_sig
	cmp randomNumSig,1
	je L2_sig
	cmp randomNumSig,2
	je L3_sig
	cmp randomNumSig,3
	je L4_sig
	cmp randomNumSig,4
	je L5_sig
	cmp randomNumSig,5
	je L6_sig
	cmp randomNumSig,6
	je L7_sig
	cmp randomNumSig,7
	je L8_sig
	jmp LSALIR
	L1_sig:
	incializalizar_t_sig
	jmp LSALIR
	L2_sig:
	incializalizar_Linea_sig
	jmp LSALIR
	L3_sig:
	incializalizar_Lizq_sig
	jmp LSALIR
	L4_sig:
	incializalizar_Lder_sig
	jmp LSALIR
	L5_sig:
	incializalizar_cuadrado_sig
	jmp LSALIR
	L6_sig:
	incializalizar_Zizq_sig
	jmp LSALIR
	L7_sig:
	incializalizar_Zder_sig
	jmp LSALIR
	L8_sig:	
	incializalizar_pto_sig


	LSALIR:
	;---------------------------------------
	;println p8
	;mostrarSig
	;call delay4

	mostarsig
	mostrarSig
	;call delay4
	mov valiarponer_sacar_sig,1b
	mostarsig
	mov valiarponer_sacar_sig,0b
endm

ponerpieza macro
	LOCAL L1,L2,L3,L4,SALIR
	cmp isespecial,0b
	je L1
	jmp L2

	L1:

	colocarpunto posy1,posx1
	colocarpunto posy2,posx2
	colocarpunto posy3,posx3
	colocarpunto posy4,posx4
	;mostrartaTempb
	
	jmp SALIR

	L2:

	cmp valsobreposicion,1b
	jne L4

	L3:
	espaciocolumna 

	L4:
	colocarpunto posy1,posx1
	

	SALIR:
endm

espaciocolumna macro 
	LOCAL L1,L2,L3,LSALIR
	push ax
	
	mov ax,posy1
	mov  posy1_l , ax
	mov  posyFinal , ax
	mov ax,posx1
    mov  posx1_l , ax

	pop ax

	L2:
	add posy1_l,1
	calcrelativo posy1_l,posx1_l
	cmp si,149d
	ja LSALIR

	mov al, tablerotmp[si]
	cmp al,"-"
	jne L2
	jmp L3

	L3:
	push ax
	mov ax,posy1_l
	mov posyFinal,ax

	pop ax
	
	jmp L2

	LSALIR:
	push ax
	mov ax,posyFinal
	mov posy1,ax
	pop ax
endm

FinJuego macro 
	LOCAL plasmarpiezaAbajo,LSALIR
	add posy1,1d
	add posy2,1d
	add posy3,1d
	add posy4,1d

	validarsobreposicion

	cmp valsobreposicion,1b
	JE plasmarpiezaAbajo
	jmp LSALIR

	plasmarpiezaAbajo:
	jmp LFinJuego

	LSALIR:
	
endm 

movaDER macro 
	LOCAL plasmarpiezaAbajo,LSALIR
	cmp posx1,9
	je LSALIR
	cmp posx2,9
	je LSALIR
	cmp posx3,9
	je LSALIR
	cmp posx4,9
	je LSALIR

	add posx1,1d
	add posx2,1d
	add posx3,1d
	add posx4,1d

	validarsobreposicion

	cmp valsobreposicion,1b
	JE plasmarpiezaAbajo
	jmp LSALIR

	plasmarpiezaAbajo:
	mov valsobreposicion,0b
	sub posx1,1d
	sub posx2,1d
	sub posx3,1d
	sub posx4,1d
	jmp L1

	LSALIR:

endm

movaIzq macro 
	LOCAL plasmarpiezaAbajo,LSALIR
	cmp posx1,0
	je LSALIR
	cmp posx2,0
	je LSALIR
	cmp posx3,0
	je LSALIR
	cmp posx4,0
	je LSALIR


	sub posx1,1d
	sub posx2,1d
	sub posx3,1d
	sub posx4,1d

	validarsobreposicion

	cmp valsobreposicion,1b
	JE plasmarpiezaAbajo
	jmp LSALIR

	plasmarpiezaAbajo:
	mov valsobreposicion,0b
	add posx1,1d
	add posx2,1d
	add posx3,1d
	add posx4,1d
	jmp L1

	LSALIR:

endm

movabajo macro 
    LOCAL plasmarpiezaAbajo,LSALIR,L1
    add posy1,1d
    add posy2,1d
    add posy3,1d
    add posy4,1d

    validarsobreposicion

    cmp valsobreposicion,1b
    JE plasmarpiezaAbajo
    jmp LSALIR

    plasmarpiezaAbajo:

    sub posy1,1d
    sub posy2,1d
    sub posy3,1d
    sub posy4,1d

    cmp posy1,3d
    jbe L1
    cmp posy2,3d
    jbe L1
    cmp posy3,3d
    jbe L1
    cmp posy4,3d
    jbe L1


    ponerpieza
    mov valsobreposicion,0b

    cmp posy1,3
    jbe plasmarpiezaAbajo
    cmp posy2,3
    jbe plasmarpiezaAbajo
    cmp posy3,3
    jbe plasmarpiezaAbajo
    cmp posy4,3
    jbe plasmarpiezaAbajo
    mostrartaTempb
    jmp Iniciar

    L1:
    jmp LFinJuego

    LSALIR:

endm

calcrelativo macro posy,posx
	push ax
	mov ax,posy
	mov bx,10d
	mul bx
	add ax,posx
	mov si,ax
	pop ax
endm

colocarpunto macro posy,posx
	LOCAL L1,L2,C0,C1,C2,C3,C4,C5,C6,C7,LSALIR
	calcrelativo posy,posx
	cmp valiarponer_sacar,0b
	je L1
	jmp L2

	L1:
	cmp randomNum,0
	je C0
	cmp randomNum,1
	je C1
	cmp randomNum,2
	je C2
	cmp randomNum,3
	je C3
	cmp randomNum,4
	je C4
	cmp randomNum,5
	je C5
	cmp randomNum,6
	je C6
	cmp randomNum,7
	je C7

	C0:
	mov tablerotmp[si],"A"
	jmp LSALIR
	C1:
	mov tablerotmp[si],"B"
	jmp LSALIR
	C2:
	mov tablerotmp[si],"C"
	jmp LSALIR
	C3:
	mov tablerotmp[si],"D"
	jmp LSALIR
	C4:
	mov tablerotmp[si],"E"
	jmp LSALIR
	C5:
	mov tablerotmp[si],"F"
	jmp LSALIR
	C6:
	mov tablerotmp[si],"G"
	jmp LSALIR
	C7:
	mov tablerotmp[si],"H"
	jmp LSALIR

	L2:
	mov tablerotmp[si],"-"

	LSALIR:
endm

mostarsig macro 
	colocarpuntoSig posy1_Sig,posx1_Sig
	colocarpuntoSig posy2_Sig,posx2_Sig
	colocarpuntoSig posy3_Sig,posx3_Sig
	colocarpuntoSig posy4_Sig,posx4_Sig
endm

colocarpuntoSig macro posy,posx
	LOCAL L1,L2,LSALIR
	linealizarSig posy,posx
	cmp valiarponer_sacar_sig,0b
	je L1
	jmp L2

	L1:
	mov piezasig[si],"*"
	jmp LSALIR

	L2:
	mov piezasig[si],"-"

	LSALIR:
endm

linealizarSig macro posy,posx
	LOCAL L1,L2,L3,L4,L5
	push ax
	mov ax,posy
	mov bx,4d
	mul bx
	add ax,posx
	mov si,ax
	pop ax
endm

incializalizar_t macro
	mov isespecial,0b
	mov posy1,2d;arriba
    mov posx1,1d

    mov posy2,3d
    mov posx2,0d;mas izq

    mov posy3,3d
    mov posx3,1d;enmedio abajo

    mov posy4,3d
    mov posx4,2d;mas der

	generarespecificrandom 8
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
endm

incializalizar_Lizq macro
	mov isespecial,0b
	mov posy1,1d
    mov posx1,0d;iz

    mov posy2,1d
    mov posx2,1d;arriba

    mov posy3,2d
    mov posx3,1d;enmedio

    mov posy4,3d
    mov posx4,1d;abajo

	generarespecificrandom 9
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
	
endm

incializalizar_Lder macro
	mov isespecial,0b
	mov posy1,1d
    mov posx1,1d;der

    mov posy2,1d
    mov posx2,0d;arriba

    mov posy3,2d
    mov posx3,0d;enmedio

    mov posy4,3d;abajo
    mov posx4,0d

	generarespecificrandom 9
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
	
endm

incializalizar_cuadrado macro
	mov isespecial,0b
	mov posy1,2d
    mov posx1,1d

    mov posy2,3d
    mov posx2,1d

    mov posy3,2d
    mov posx3,0d

    mov posy4,3d
    mov posx4,0d

	generarespecificrandom 9
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
	
endm

incializalizar_Zizq macro
	mov isespecial,0b
	mov posy1,2d
    mov posx1,0d;iz arriba

    mov posy2,2d
    mov posx2,1d;der arriba

    mov posy3,3d
    mov posx3,1d;iz abajo

    mov posy4,3d
    mov posx4,2d;der abajo

	generarespecificrandom 8
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
endm

incializalizar_Zder macro
	mov isespecial,0b
	mov posy1,3d
    mov posx1,0d ;iz abajo

    mov posy2,3d
    mov posx2,1d ;der abajo

    mov posy3,2d
    mov posx3,1d ;iz arriba

    mov posy4,2d
    mov posx4,2d ;der arriba

	generarespecificrandom 8
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
	
endm

incializalizar_Linea macro
	mov isespecial,0b
	mov posy1,0d
    mov posx1,0d;arriba

    mov posy2,1d
    mov posx2,0d

    mov posy3,2d
    mov posx3,0d

    mov posy4,3d
    mov posx4,0d;abajo

	generarespecificrandom 10
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax
	
endm

incializalizar_pto macro
	mov isespecial,1b
	mov posy1,3d
    mov posx1,0d;pto

    mov posy2,3d
    mov posx2,0d

    mov posy3,3d
    mov posx3,0d

    mov posy4,3d
    mov posx4,0d

	generarespecificrandom 10
	xor ax,ax
	mov al,randomPos
	add posx1,ax
	add posx2,ax
	add posx3,ax
	add posx4,ax

endm

generarespecificrandom macro max
    call delay
    mov ah,0h
    int 1ah
    mov ax,dx
    mov dx,0
    mov bx,max
    div bx
    mov randomPos,dl
endm

incializalizar_t_sig macro
	mov posy1_Sig,2d;arriba
    mov posx1_Sig,1d

    mov posy2_Sig,3d
    mov posx2_Sig,0d;mas izq

    mov posy3_Sig,3d
    mov posx3_Sig,1d;enmedio abajo

    mov posy4_Sig,3d
    mov posx4_Sig,2d;mas der

endm

incializalizar_Lizq_sig macro

	mov posy1_Sig,1d
    mov posx1_Sig,0d;iz

    mov posy2_Sig,1d
    mov posx2_Sig,1d;arriba

    mov posy3_Sig,2d
    mov posx3_Sig,1d;enmedio

    mov posy4_Sig,3d
    mov posx4_Sig,1d;abajo
endm

incializalizar_Lder_sig macro
	mov posy1_Sig,1d
    mov posx1_Sig,1d;der

    mov posy2_Sig,1d
    mov posx2_Sig,0d;arriba

    mov posy3_Sig,2d
    mov posx3_Sig,0d;enmedio

    mov posy4_Sig,3d;abajo
    mov posx4_Sig,0d
endm

incializalizar_cuadrado_sig macro
	
	mov posy1_Sig,2d
    mov posx1_Sig,1d

    mov posy2_Sig,3d
    mov posx2_Sig,1d

    mov posy3_Sig,2d
    mov posx3_Sig,0d

    mov posy4_Sig,3d
    mov posx4_Sig,0d
	
endm

incializalizar_Zizq_sig macro
	
	mov posy1_Sig,2d
    mov posx1_Sig,0d;iz arriba

    mov posy2_Sig,2d
    mov posx2_Sig,1d;der arriba

    mov posy3_Sig,3d
    mov posx3_Sig,1d;iz abajo

    mov posy4_Sig,3d
    mov posx4_Sig,2d;der abajo
endm

incializalizar_Zder_sig macro
	
	mov posy1_Sig,3d
    mov posx1_Sig,0d ;iz abajo

    mov posy2_Sig,3d
    mov posx2_Sig,1d ;der abajo

    mov posy3_Sig,2d
    mov posx3_Sig,1d ;iz arriba

    mov posy4_Sig,2d
    mov posx4_Sig,2d ;der arriba
	
endm

incializalizar_Linea_sig macro
	
	mov posy1_Sig,0d
    mov posx1_Sig,0d;arriba

    mov posy2_Sig,1d
    mov posx2_Sig,0d

    mov posy3_Sig,2d
    mov posx3_Sig,0d

    mov posy4_Sig,3d
    mov posx4_Sig,0d;abajo
endm

incializalizar_pto_sig macro
	mov posy1_Sig,3d
    mov posx1_Sig,0d;pto

    mov posy2_Sig,3d
    mov posx2_Sig,0d

    mov posy3_Sig,3d
    mov posx3_Sig,0d

    mov posy4_Sig,3d
    mov posx4_Sig,0d

endm

;------------------------------Juego end----------------------------------------

;----------------------------Inicia Video--------------------------
iniciarmodovideo macro
    push ax
    xor ax,ax
    mov ax,12h
    int 10h
    pop ax
endm

cambiaravideo macro
	push dx
	mov dx,0a000h
	mov es,dx
	pop dx
endm

entraramodovideo macro;Cambio a modo video
    ;limpiarpantallag
	;volver a entrar al modo video
    push ax
    mov ah,0
    mov al,13h
    int 10h
    pop ax
endm

regresaramodotexto macro
    push ax
    mov ax,3h
    int 10h
    pop ax
endm

pintarpixelpro macro x,y,color
    mov ah,0ch  ;Escribir un pixel en la coordenada
    mov al,color   ;Color
    mov cx,y  ;Coordenada Y
    mov dx,x  ;Coordenada X
    int 10h
endm

pintarpixel MACRO x, y, color
    ; GUARDAR REGISTROS
    PUSH AX
    PUSH BX
    PUSH DX
    PUSH SI

    ; P = (y * 320) + x
    MOV AX, y
    MOV BX, 320
    MUL BX
    ADD AX, x

    ; APLICAR ACCIÓN
    MOV SI, AX
	
    MOV BL, color
    MOV ES:[SI], BL

    ; OBTENER REGISTROS
    POP SI
    POP DX
    POP BX
    POP AX
ENDM

dibujarcuadro MACRO x, y, ancho, alto, color
    LOCAL RECORRER_X, RECORRER_Y
    ; INICIALIZAR FOR
    MOV CX, ancho
    MOV AX, x
    MOV coord_x, AX

    RECORRER_X:
        ; GUARDAR CONTADOR
        PUSH CX

        ; INICIALIZAR FOR
        MOV CX, alto
        MOV AX, y
        MOV coord_y, AX

        RECORRER_Y:
            ; PINTAR PIXEL
            pintarPixel coord_x, coord_y, color

            ; SIGUIENTE FILA
            INC coord_y
            LOOP RECORRER_Y

        ; OBTENER CONTADOR
        POP CX

        ; SIGUIENTE COLUMNA
        INC coord_x
        LOOP RECORRER_X
ENDM

dibujarmarco macro
	; -------------X----------------
	mov contadormarco,20d
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	dibujarcuadro 0,contadormarco,180,1,0
	add contadormarco,18
	; -------------Y----------------
	mov contadormarco,0
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18
	dibujarcuadro contadormarco,20,1,180,0
	add contadormarco,18

	; -------------X----------------
	mov contadormarco,129
	dibujarcuadro 200,contadormarco,54,1,0
	add contadormarco,18
	dibujarcuadro 200,contadormarco,54,1,0
	add contadormarco,18
	dibujarcuadro 200,contadormarco,54,1,0
	add contadormarco,18
	dibujarcuadro 200,contadormarco,54,1,0
	
	; --------------------------Y SIG------------------
	mov contadormarco,200
	dibujarcuadro contadormarco,129,1,72,0
	add contadormarco,18
	dibujarcuadro contadormarco,129,1,72,0
	add contadormarco,18
	dibujarcuadro contadormarco,129,1,72,0
	add contadormarco,18
	dibujarcuadro contadormarco,129,1,72,0
	
	
	dibujarcuadro 180,20,130,1,100
	dibujarcuadro 180,38,130,1,100
	dibujarcuadro 180,62,130,1,100
	dibujarcuadro 180,85,130,1,100
	dibujarcuadro 180,112,130,1,100
	
	dibujarcuadro 180,20,1,181,100
	dibujarcuadro 310,20,1,181,100


	pintartexto msgpalabrapuntaje,3,25,89

	Num2str punteo,aimprimir
	pintartexto aimprimir,3,35,89
	
	pintartexto msgpalabranivel,6,25,89
	
	Num2str nivelactualnumero,aimprimir
	pintartexto aimprimir,6,35,89
	
	pintartexto msgpalabratiempo,9,25,89

	
	Num2str literalminuto,stringminuto
	pintartexto stringminuto,9,32,89
	
	pintartexto msgdospuntos,9,34,89
	
	calcularsegundo
	Num2str literalsegundo,stringsegundo
	pintartexto stringsegundo,9,35,89
	
	pintartexto msgpalabracentisegundo,12,25,89
	
	calcularcentisegundo
	Num2str literalcentisegundo,stringcentisegundo
	pintartexto stringcentisegundo,12,32,89
endm

; MOSTRAR CADENA EN PANTALLA DE VIDEO ___________________________________________________
pintartexto MACRO cadena, fila, columna, color
    ; GUARDAR REGISTROS
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH BP

    ; AJUSTAR CONFIGURACIÓN
    MOV AL, 1
    MOV BH, 0
    MOV BL, color
    MOV CX, LENGTHOF cadena
    MOV DH, fila
    MOV DL, columna

    ; CAMBIAR A MEMORIA DE DS
    memoriaDS

    ; APLICAR INTERRUPCIÓN
    MOV BP, OFFSET cadena
    MOV AH, 13H
    INT 10H

    ; CAMBIAR A MEMORIA DE VIDEO
    cambiaravideo

    ; OBTENER REGITROS
    POP BP
    POP DX
    POP CX
    POP BX
    POP AX
ENDM

imprimirenvideo macro
	limpiarpantalla
	call cambiartodoavideo
endm

memoriaDS MACRO
    ; GUARDAR REGISTRO
    PUSH DX

    ; APLICAR ACCIÓN
    MOV DX, @DATA
    MOV ES, DX

    ; OBTENER REGISTRO
    POP DX
ENDM

;--------------------------------------------------------------
;-----------------------macros generales-----------------------
;--------------------------------------------------------------

mostrarSig macro
    LOCAL Leetab,Imprimirtab,Salirmostrartab,guion,noguion,finpintar

    mov si,00
    mov di,00
	mov video_y,130d
	mov video_x,0
    mov temporal_video,00
    Leetab:
		push ax
		mov ax,si
		sub ax,temporal_video
		mov bx,18d
		mul bx
		mov video_x,ax
		add video_x,200d
		pop ax

        ;printChar piezasig[si]
        cmp piezasig[si],"-"
		jne noguion
		guion:
		mov color_video,0
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar
		noguion:
		mov color_video,5
		dibujarcuadro video_x,video_y,18,18,color_video
		
        finpintar:
		inc si
        inc di
        cmp di,4
        je Imprimirtab
        jne Leetab
    Imprimirtab:
		mov temporal_video,si
		add video_y,18d 
        xor di,di
        ;printChar 10
        cmp si,16
        je Salirmostrartab
        jmp Leetab
    Salirmostrartab:
    
endm 

mostrartaTempb macro
    LOCAL Leetab,Imprimirtab,Salirmostrartab,finpintar,guion,noguion,colorte,colorlinea,colorLder,colorLizq,colorZizq,colorZder,colorpunto

    mov si,50
    mov di,00
	mov video_y,20
	mov video_x,00
	mov temporal_video,50
    
    Leetab:
		push ax
		mov ax,si
		sub ax,temporal_video
		mov bx,18d
		;add ax,1d
		mul bx
		mov video_x,ax
		;PAnyNumber
		;call delay2
		pop ax
        ;printChar tablerotmp[si]
		cmp tablerotmp[si],"-"
		jne noguion
		guion:
		mov color_video,20
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar
		noguion:
		cmp tablerotmp[si],"A"
		je colorlinea
		cmp tablerotmp[si],"B"
		je colorZizq
		cmp tablerotmp[si],"C"
		je colorZder
		cmp tablerotmp[si],"D"
		je colorLizq
		cmp tablerotmp[si],"E"
		je colorLder
		cmp tablerotmp[si],"F"
		je colorte
		cmp tablerotmp[si],"G"
		je colorpunto
		mov color_video,46
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorlinea:
		mov color_video,56
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorZizq:
		mov color_video,66
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorZder:
		mov color_video,76
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorLizq:
		mov color_video,86
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorLder:
		mov color_video,96
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorte:
		mov color_video,106
		dibujarcuadro video_x,video_y,18,18,color_video
		jmp finpintar

		colorpunto:
		mov color_video,116
		dibujarcuadro video_x,video_y,18,18,color_video
		
        finpintar:
		push si
		push di
		;call delay2
		pop di
		pop si
		inc si
        inc di
        cmp di,10
        je Imprimirtab
        jne Leetab
    Imprimirtab:
		mov temporal_video,si
		add video_y,18d
        xor di,di
        ;printChar 10
        cmp si,150
        je Salirmostrartab
        jmp Leetab
    Salirmostrartab:
	dibujarmarco
	;AQUI ES DONDE VOY A PINTAR EN LUGAR DE IMPRIMIR
endm 

mostrartab macro
    LOCAL Leetab,Imprimirtab,Salirmostrartab

    mov si,00
    mov di,00
    
    Leetab:
        printChar tablero[si]
        inc si
        inc di
        cmp di,10
        je Imprimirtab
        jne Leetab
    Imprimirtab:
        xor di,di
        printChar 10
        cmp si,100
        je Salirmostrartab
        jmp Leetab
    Salirmostrartab:
    
endm 

pedirtecla macro 
	xor ah,ah
	int 16h
endm

Fecha macro
	push bx
	xor bx,bx
	
	mov ah,2ah
	int 21H
	mov literalanio,cx
	mov bl,dh
	mov literalmes,bx
	mov bl,dl
	mov literaldia,bx
	
	Num2Str literalanio,stringanio
	mov contadorfecha,00
	Num2Str literalmes,stringmes
	mov contadorfecha,00
	Num2Str literaldia,stringdia
	mov contadorfecha,00
	pop bx
endm

Hora macro
	push bx
	mov ah,02ch
	int 21H
	mov bl,ch
	mov literalhora,bx
	mov bl,cl
	mov literalminuto,bx
	mov bl,dh
	mov literalsegundo,bx
	Num2Str literalhora,stringhora
	mov contadorfecha,00
	Num2Str literalminuto,stringminuto
	mov contadorfecha,00
	Num2Str literalsegundo,stringsegundo
	mov contadorfecha,00
	pop bx
endm

Num2str macro numero,stringvar
	local cNumerador,Convertir 
	push si
    push ax
    push bx
    push dx
    mov contadorfecha,0d
    xor si,si
    mov bx,10d
    mov ax,numero
    cNumerador:
        xor dx,dx
        div bx
        push dx
        inc contadorfecha
        cmp ax,0
        jne cNumerador
    mov si,offset stringvar
    Convertir:
        pop dx
        add dx,48d
        mov [si],dx
        inc si
        dec contadorfecha
        cmp contadorfecha,0
        jne Convertir
    pop dx
    pop bx
    pop ax
    pop si
	;println stringvar
endm

calcularsegundo macro
	local L1,fin
	mov ah,2ch
	int 21H

	cmp dh,ultimosegundo
	je fin
	L1:
		mov ultimosegundo,dh
		inc literalsegundo
		cmp literalsegundo,60d
		jne fin 
		mov literalsegundo,0
		inc literalminuto
		;Num2str literalminuto,stringminuto
		;pintartexto stringminuto,8,32,89
	fin:
endm

calcularsegundopausa macro
	local L1,fin
	mov ah,2ch
	int 21H

	cmp dh,ultimosegundoenpausa
	je fin
	L1:
		mov ultimosegundoenpausa,dh
		inc literalsegundoenpausa
		cmp literalsegundoenpausa,60d
		jne fin 
		mov literalsegundoenpausa,0
		inc literalminutoenpausa
		;Num2str literalminuto,stringminuto
		;pintartexto stringminuto,8,32,89
	fin:
endm
calcularcooldown macro
	local L1,fin,Loopeado
	push si
	xor si,si
	mov literalsegundoenpausa,00
	Loopeado:
	call delayGame
	mov ah,2ch
	int 21H

	L1:
		inc literalsegundoenpausa
		Num2str literalsegundoenpausa,aimprimir
		;println aimprimir
		cmp literalsegundoenpausa,30d
		jne Loopeado 
		mov literalsegundoenpausa,0
		;inc literalminutoenpausa
		;Num2str literalminuto,stringminuto
		;pintartexto stringminuto,8,32,89
	fin:
	pop si
endm

calcularcentisegundo macro
	local L1,fin
	mov ah,2ch
	int 21H

	L1:
		mov ultimocentisegundo,dl
		xor dx,dx
		mov dl,ultimocentisegundo
		mov literalcentisegundo,dx
		;inc literalsegundo
		;Num2str literalminuto,stringminuto
		;pintartexto stringminuto,8,32,89
	fin:
endm